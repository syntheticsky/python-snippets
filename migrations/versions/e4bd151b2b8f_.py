"""empty message

Revision ID: e4bd151b2b8f
Revises: 25c0f45ac524
Create Date: 2017-03-16 14:18:38.080559

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e4bd151b2b8f'
down_revision = '25c0f45ac524'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('topics_content', sa.Column('position', sa.SmallInteger(), nullable=True))
    conn = op.get_bind()
    conn.execute(sa.text("""update
                                 topics_content
                                 set position = 0"""))
    op.alter_column('topics_content', 'position', nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('topics_content', 'position')
    # ### end Alembic commands ###
