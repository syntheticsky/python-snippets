"""empty message

Revision ID: e7341e8e0805
Revises: 7c03ed740b12
Create Date: 2017-03-15 23:54:05.239711

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'e7341e8e0805'
down_revision = '7c03ed740b12'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('topics', sa.Column('search_vector', postgresql.TSVECTOR(), nullable=True))
    op.create_index('idx_topic_search_vector', 'topics', ['search_vector'], unique=False, postgresql_using='gin')
    op.add_column('topics_content', sa.Column('search_vector', postgresql.TSVECTOR(), nullable=True))
    op.add_column('topics_content', sa.Column('type', sa.String(length=16), nullable=True))
    op.create_index('idx_topic_content_search_vector', 'topics_content', ['search_vector'], unique=False, postgresql_using='gin')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('idx_topic_content_search_vector', table_name='topics_content')
    op.drop_column('topics_content', 'type')
    op.drop_column('topics_content', 'search_vector')
    op.drop_index('idx_topic_search_vector', table_name='topics')
    op.drop_column('topics', 'search_vector')
    # ### end Alembic commands ###
