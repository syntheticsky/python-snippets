"""empty message

Revision ID: 8121f1396c9f
Revises: 855a910397b0
Create Date: 2017-03-28 22:32:43.603068

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8121f1396c9f'
down_revision = '855a910397b0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('category', 'title', new_column_name='name')
    op.alter_column('tag', 'title', new_column_name='name')
    op.create_index('idx_tag_name', 'tag', ['name'], unique=True)
    op.drop_index('idx_tag_title', table_name='tag')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('category', 'name', new_column_name='title')
    op.alter_column('tag', 'name', new_column_name='title')
    op.create_index('idx_tag_title', 'tag', ['title'], unique=True)
    op.drop_index('idx_tag_name', table_name='tag')
    # ### end Alembic commands ###
