# My project's README

Centos7 yum install policycoreutils-python

sudo chcon -Rt httpd_sys_content_t /var/www/snippets/
setsebool -P httpd_can_network_connect 1

sudo cat /var/log/audit/audit.log | grep nginx | grep denied
sudo cat /var/log/audit/audit.log | grep nginx | grep denied | audit2allow -M mynginx
sudo semodule -i mynginx.pp

// Full text search
iconv -f koi8-r -t utf-8 < ru_RU.aff > /usr/share/pgsql/tsearch_data/russian.affix
iconv -f koi8-r -t utf-8 < ru_RU.dic > /usr/share/pgsql/tsearch_data/russian.dict

CREATE TEXT SEARCH DICTIONARY russian_ispell (
    TEMPLATE = ispell,
    DictFile = russian,
    AffFile = russian,
    StopWords = russian
);

CREATE TEXT SEARCH DICTIONARY english_ispell (
    TEMPLATE = ispell,
    DictFile = english,
    AffFile = english,
    StopWords = english
);

CREATE TEXT SEARCH CONFIGURATION ru ( COPY = russian );
CREATE TEXT SEARCH CONFIGURATION en ( COPY = english );

ALTER TEXT SEARCH CONFIGURATION ru ALTER MAPPING FOR hword, hword_part, word WITH russian_stem, russian_ispell;
ALTER TEXT SEARCH CONFIGURATION ru ALTER MAPPING FOR asciihword, asciiword, hword_asciipart WITH english_stem, english_ispell;

ALTER TEXT SEARCH CONFIGURATION en ALTER MAPPING FOR hword, hword_part, word WITH  russian_stem, russian_ispell;
ALTER TEXT SEARCH CONFIGURATION en ALTER MAPPING FOR asciihword, asciiword, hword_asciipart WITH english_stem, english_ispell;

default_text_search_config = 'ru'

CREATE OR REPLACE FUNCTION topics_vector_update() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        NEW.search_vector = setweight(to_tsvector('ru', coalesce(NEW.title,'')), 'A') || setweight(to_tsvector('ru', coalesce(NEW.description,'')), 'C');
    END IF;
    IF TG_OP = 'UPDATE' THEN
        IF NEW.title <> OLD.title OR NEW.description <> OLD.description THEN
            NEW.search_vector = setweight(to_tsvector('ru', coalesce(NEW.title,'')), 'A') || setweight(to_tsvector('ru', coalesce(NEW.description,'')), 'C');
        END IF;
    END IF;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE ON topics
FOR EACH ROW EXECUTE PROCEDURE topics_vector_update();

sudo systemctl restart snippets-web

python 3.6

RUN apk update && \
 apk add postgresql-libs && \
 apk add --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

pip install --upgrade --force-reinstall -r requirements/base.txt