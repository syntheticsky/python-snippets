import flask_cors
import logging.handlers
import traceback
from time import strftime

import flask
# import redis
from werkzeug.contrib.cache import RedisCache, SimpleCache

from app.bundle.api.action.action_registry import ActionRegistry
from app.bundle.api.routing.routing_converter import (HexUUIDConverter, RegexConverter)
from app.ext import bcrypt, db

application = flask.Flask(__name__)
flask_cors.CORS(application)

application.url_map.converters['uuidhex'] = HexUUIDConverter
application.url_map.converters['regex'] = RegexConverter
# application.config.from_object(os.environ['APP_FLASK_SETTINGS'])
application.config.from_object('config.DevelopmentConfig')
# application.config.from_object('config.ProductionConfig')

# if not application.debug:
handler = logging.handlers.RotatingFileHandler('logs/app.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.DEBUG)
application.logger.addHandler(handler)
application.logger.setLevel(logging.DEBUG)

bcrypt.init_app(application)

# Redis
# pool = redis.ConnectionPool(host=application.config.get('REDIS_URL'), port=application.config.get('REDIS_PORT'),
#                             db=application.config.get('REDIS_DB'), decode_responses=True)
# redis_db = redis.Redis(connection_pool=pool)

memberRegistry = ActionRegistry()
authRegistry = ActionRegistry()
publicRegistry = ActionRegistry()

db.init_app(application)
with application.test_request_context():
    db.create_all()

if application.config.get('DEVELOPMENT'):
    cache = SimpleCache()
else:
    cache = RedisCache(host=application.config.get('REDIS_URL'), port=application.config.get('REDIS_PORT'),
                       db=application.config.get('REDIS_DB'))


@application.before_first_request
def before_request():
    db.session()


@application.after_request
def after_request(response):
    timestamp = strftime('[%Y-%b-%d %H:%M]')
    application.logger.info('%s %s %s %s %s %s',
                             timestamp, flask.request.remote_addr, flask.request.method,
                             flask.request.scheme, flask.request.path, response.status)
    return response


@application.errorhandler(Exception)
def exceptions(e):
    tb = traceback.format_exc()
    timestamp = strftime('[%Y-%b-%d %H:%M]')
    application.logger.error('%s %s %s %s %s 5xx INTERNAL SERVER ERROR\n%s',
                             timestamp, flask.request.remote_addr, flask.request.method,
                             flask.request.scheme, flask.request.path, tb)
    return str(e)


@application.teardown_request
def teardown_request(exception):
    if exception:
        db.session.rollback()
        db.session.remove()
    db.session.remove()


import app.bundle.api.controllers as api
import app.bundle.core.controllers as core
import app.bundle.topics.controllers as topics
import app.bundle.users.controllers as user

application.register_blueprint(api.apiBlueprint)
application.register_blueprint(core.coreBlueprint)
application.register_blueprint(topics.topicBlueprint)
application.register_blueprint(user.userBlueprint)
