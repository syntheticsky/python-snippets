import datetime
import re
import uuid

import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.orm.exc

import app
from app.bundle.api.authetication.authentication import current_user
from app.bundle.api.cache.cache import cached_action
from app.bundle.api.components.model_builder.model_builder import ModelBuilder
from app.bundle.api.components.paginator.paginator import Pagination
from app.bundle.api.components.utils import entity_sequences_diff
from app.bundle.api.exception.api_exception import ApiException
from app.bundle.api.server.response.response import ApiResponse
from app.bundle.topics.models import (Category, Tag, Topic, TopicContent, TopicShareLink, TopicTags)
from app.bundle.topics.request.request import (
    CategoryActionUUIDRequest,
    CategoryCreateRequest,
    CategoryListRequest,
    CategorySearchRequest,
    CategoryUpdateRequest,
    HistoryViewRequest,
    TopicActionSharedRequest,
    TopicActionUUIDRequest,
    TopicCreateRequest,
    TopicListRequest,
    TopicSearchRequest,
    TopicUpdateRequest,
    TagCreateRequest,
    TagListRequest,
    TagSearchRequest,
    TagUpdateRequest,
)
from app.bundle.topics.request.validator import (
    CategoryActionUUIDValidator,
    CategoryCreateValidator,
    CategoryListValidator,
    CategorySearchValidator,
    CategoryUpdateValidator,
    HistoryViewValidator,
    TopicActionSharedValidator,
    TopicActionUuidValidator,
    TopicCreateValidator,
    TopicListValidator,
    TopicSearchValidator,
    TopicUpdateValidator,
    TagCreateValidator,
    TagListValidator,
    TagSearchValidator,
    TagUpdateValidator,
)


@app.publicRegistry.endpoint('history.list', HistoryViewValidator)
def history_view(api_request: HistoryViewRequest):
    """
    Action to return users shared topics
    :param api_request:
    :return:
    """
    query = Topic.query.distinct().join(TopicShareLink, Topic.shared, isouter=True).filter(
        sqlalchemy.and_(
            Topic.uuid.in_(api_request.uuid),
            sqlalchemy.or_(
                Topic.public.is_(True),
                TopicShareLink.topic_id.isnot(None),
            )
        )
    )

    pagination = Pagination(query, api_request.page, api_request.limit)

    return ApiResponse(pagination.get_model(['Default', 'Shared']))


@app.publicRegistry.endpoint('topic.view.shared', TopicActionSharedValidator)
def topic_view(api_request: TopicActionSharedRequest):
    """
    Action for topic view by shared token.
    :param api_request:
    :return:
    """
    try:
        topic = Topic.query.join(TopicShareLink, Topic.shared).filter(
            sqlalchemy.and_(
                Topic.id == TopicShareLink.topic_id,
                TopicShareLink.token == TopicShareLink.token_to_binary(api_request.token)
            )
        ).one_or_none()

        if topic is None:
            raise ApiException('Topic not found', status=404)

        return ApiResponse(ModelBuilder.build_model(topic, []))
    except sqlalchemy.orm.exc.MultipleResultsFound as e:
        raise ApiException('Multiple result found', status=500)


@app.memberRegistry.endpoint('topic.view', TopicActionUuidValidator)
def topic_view(api_request: TopicActionUUIDRequest):
    """
    Action for topic view.
    :param api_request:
    :return:
    """
    try:
        user = current_user()
        topic = Topic.query.join(TopicShareLink, Topic.shared, isouter=True).filter(
            Topic.uuid == uuid.UUID(api_request.uuid)
        ).one_or_none()

        if topic is None:
            raise ApiException('Topic not found with uuid "{}"'.format(api_request.uuid), status=404)
        if topic.user_id != user.id:
            raise ApiException('Access denied', status=403)

        return ApiResponse(ModelBuilder.build_model(topic, ['Default', 'Shared']))
    except sqlalchemy.orm.exc.MultipleResultsFound as e:
        raise ApiException(str(e), status=500)


@app.memberRegistry.endpoint('topic.list', TopicListValidator)
def topic_list(api_request: TopicListRequest):
    """
    Action to return users topics
    :param api_request: 
    :return:
    """
    user = current_user()

    query = Topic.query.distinct().join(TopicShareLink, Topic.shared, isouter=True).filter(Topic.user_id == user.id)
    if api_request.tags:
        query = query.join(Tag, Topic.tags)\
            .filter(Tag.uuid.in_(api_request.tags))
    if api_request.categories:
        query = query.join(Category, Topic.category)\
            .filter(Category.uuid.in_(api_request.categories))
    # query = query.options(joinedload('attachments'))
    pagination = Pagination(query, api_request.page, api_request.limit)

    return ApiResponse(pagination.get_model(['Default', 'Shared']))


@app.memberRegistry.endpoint('topic.create', TopicCreateValidator)
def topic_create(api_request: TopicCreateRequest):
    """
    Action to create topic
    :param api_request: 
    :return: 
    """
    user = current_user()
    topic = Topic()
    topic.title = api_request.title
    topic.description = api_request.description
    topic.user_id = user.id
    topic.content = api_request.content
    topic.attachments = []
    topic.extra_content = []
    topic.updated_at = datetime.datetime.utcnow()
    topic.public = api_request.public or False

    # Process tags
    for tag in api_request.tags:
        if tag.get('id'):
            topic_tag = Tag.query.get(tag.get('id'))

            if topic_tag is not None:
                topic.tags.append(topic_tag)
        elif tag.get('title'):
            topic_tag = Tag()
            topic_tag.title = tag.get('title')
            topic.tags.append(topic_tag)

    for position, content in enumerate(api_request.extra_content):
        topic_content = TopicContent()
        topic_content.name = content.get('name')
        topic_content.value = content.get('value')
        topic_content.type = content.get('type')
        topic_content.position = position
        topic.content.append(topic_content)

    if api_request.category:
        if api_request.category.get('id'):
            if topic.validate_category_id(api_request.category.get('id')):
                category = Category.query.get(api_request.category.get('id'))

                if category is None:
                    raise ApiException('Category not found with id "{}"'.format(api_request.category.get('id')),
                                       status=404)
                if category.user_id != user.id:
                    raise ApiException('Access denied', status=403)

                topic.category = category

        elif api_request.category.get('name'):
            if topic.validate_category_name(api_request.category.get('name')):
                category = Category.query.filter(Category.user_id == user.id) \
                    .filter(Category.name == api_request.category.get('name'))

                if category is None:
                    category = Category()
                    category.name = api_request.category.get('name')
                    category.user_id = user.id

                topic.category = category
        else:
            category = Category.query.filter(
                sqlalchemy.and_(Category.name == Category.UNCATEGORIZED, Category.user_id == user.id)
            )
            topic.category = category
    else:
        category = Category.query.filter(
            sqlalchemy.and_(Category.name == Category.UNCATEGORIZED, Category.user_id == user.id)
        )
        topic.category = category

    app.db.session.add(topic)
    app.db.session.commit()

    return ApiResponse(ModelBuilder.build_model(topic))


@app.memberRegistry.endpoint('topic.update', TopicUpdateValidator)
def topic_update(api_request: TopicUpdateRequest):
    """
    Action to update topic
    :param api_request: 
    :return: 
    """
    user = current_user()
    topic = Topic.query.filter(Topic.uuid == uuid.UUID(api_request.uuid)).one_or_none()

    if topic is None:
        raise ApiException('Topic not found with uuid "{}"'.format(api_request.uuid), status=404)
    if topic.user_id != user.id:
        raise ApiException('Access denied', status=403)

    topic.title = api_request.title
    topic.description = api_request.description
    topic.content = api_request.content
    topic.user_id = user.id
    topic.updated_at = datetime.datetime.utcnow()
    topic.public = api_request.public or False

    if api_request.tags:
        # tags to remove that not requested by id or name
        remove_tags = list(
            entity_sequences_diff(
                list(entity_sequences_diff(topic.tags, api_request.tags)),
                api_request.tags,
                'name'
            )
        )
        for tag in remove_tags:
            topic.tags.remove(tag)
        # actual requested tags. Diff from topic.tags by id and name
        api_request.tags = list(
            entity_sequences_diff(
                list(entity_sequences_diff(api_request.tags, topic.tags, 'name')),
                topic.tags
            )
        )

        if api_request.tags:
            tags_ids = [tag.get('id') for tag in api_request.tags if tag.get('id')]
            if tags_ids:
                tags_by_id = Tag.query.filter(Tag.id.in_(tags_ids)).all()
                for tag in tags_by_id:
                    topic.tags.append(tag)
            tags_names = [tag.get('name') for tag in api_request.tags if not tag.get('id')]
            if tags_names:
                tags_by_name = Tag.query.filter(Tag.name.in_(tags_names)).all()
                for tag in tags_by_name:
                    tags_names.remove(tag.name)
                    topic.tags.append(tag)
                for name in tags_names:
                    topic_tag = Tag()
                    topic_tag.name = name
                    topic.tags.append(topic_tag)
    else:
        topic.tags.clear()

    if api_request.extra_content:
        extras_remove = list(entity_sequences_diff(topic.extra_content, api_request.extra_content))
        extras_actual = list(entity_sequences_diff(api_request.extra_content, topic.extra_content))
        for extra_remove in extras_remove:
            topic.extra_content.remove(extra_remove)
        for position, content in enumerate(extras_actual):
            if content.get('id'):
                topic_content = TopicContent.query.get(content.get('id'))
                if topic_content is not None:
                    topic.extra_content.append(topic_content)
            else:
                topic_content = TopicContent()
                topic.extra_content.append(topic_content)

            if topic_content is not None:
                topic_content.name = content.get('name')
                topic_content.value = content.get('value')
                topic_content.type = content.get('type')
                topic_content.position = position
    else:
        topic.extra_content.clear()

    if api_request.category:
        if api_request.category.get('id'):
            if topic.validate_category_id(api_request.category.get('id')):
                category = Category.query.get(api_request.category.get('id'))

                if category is None:
                    raise ApiException('Category not found with id "{}"'.format(api_request.category.get('id')),
                                       status=404)
                if category.user_id != user.id:
                    raise ApiException('Access denied', status=403)

                topic.category = category

        elif api_request.category.get('name'):
            if topic.validate_category_name(api_request.category.get('name')):
                category = Category.query.filter(Category.user_id == user.id) \
                    .filter(Category.name == api_request.category.get('name'))

                if category is None:
                    category = Category()
                    category.name = api_request.category.get('name')
                    category.user_id = user.id

                topic.category = category
        else:
            category = Category.query.filter(
                sqlalchemy.and_(Category.name == Category.UNCATEGORIZED, Category.user_id == user.id)
            )
            topic.category = category
    else:
        category = Category.query.filter(
            sqlalchemy.and_(Category.name == Category.UNCATEGORIZED, Category.user_id == user.id)
        )
        topic.category = category

    app.db.session.commit()

    return ApiResponse(ModelBuilder.build_model(topic))


@app.memberRegistry.endpoint('topic.remove', TopicActionUuidValidator)
def topic_delete(api_request: TopicActionUUIDRequest):
    """
    Action to remove topic
    :param api_request: 
    :return: 
    """
    user = current_user()
    topic = Topic.query.filter(Topic.uuid == uuid.UUID(api_request.uuid)).one_or_none()

    if topic is None:
        raise ApiException('Topic not found with uuid "{}"'.format(api_request.uuid), status=404)
    if topic.user_id != user.id:
        raise ApiException('Access denied', status=403)

    app.db.session.delete(topic)
    app.db.session.commit()

    return ApiResponse({'message': 'Topic removed'})


@app.memberRegistry.endpoint('topic.search', TopicSearchValidator)
def topic_search(api_request: TopicSearchRequest):
    """
    Action to search users topics
    :param api_request:
    :return:
    """
    user = current_user()

    search_query = api_request.query.strip(' &|@!')
    search_query1 = re.sub(r"(\w+)", r"\1:* | ", search_query).strip(' &|')
    search_query2 = re.sub(r"(\w+)", r"\1:* & ", search_query).strip(' &|')

    query = Topic.query.distinct().filter(Topic.user_id == user.id)
    query = query.filter(sqlalchemy.or_(
        Topic.search_vector.match(search_query1, postgresql_regconfig='en'),
        Topic.search_vector.match(search_query2, postgresql_regconfig='en')
    ))
    # query = query.filter(text("topics.search_vector @@ to_tsquery('en', :search_query)"))
    # query = query.params(search_query=search_query + ":*")

    if api_request.tags:
        query = query.join(Tag, Topic.tags)\
            .filter(Tag.uuid.in_(api_request.tags))
    if api_request.categories:
        query = query.join(Category, Topic.category)\
            .filter(Category.uuid.in_(api_request.categories))
    # query = query.options(joinedload('attachments'))
    pagination = Pagination(query, api_request.page, api_request.limit)

    return ApiResponse(pagination.get_model())


@app.memberRegistry.endpoint('topic.share.link', TopicActionUuidValidator)
@cached_action()
def topic_share_link(api_request: TopicActionUUIDRequest):
    """
    Action to share topic by link.
    :param api_request:
    :return:
    """
    try:
        user = current_user()
        topic = Topic.query.join(TopicShareLink, Topic.shared, isouter=True).filter(
            Topic.uuid == uuid.UUID(api_request.uuid)).options(sqlalchemy.orm.joinedload('shared')).one_or_none()

        if topic is None:
            raise ApiException('Topic not found with uuid "{}"'.format(api_request.uuid), status=404)
        if topic.user_id != user.id:
            raise ApiException('Access denied', status=403)

        if not topic.shared:
            topic_share_token= TopicShareLink(topic.id)
            topic_share_token.set_token(topic.build_hash())
            app.db.session.add(topic_share_token)
            app.db.session.commit()
            topic.shared = topic_share_token

        return ApiResponse({'shared_token': topic.shared.get_token()}, status=200, message='Snippet shared successfully.')
    except sqlalchemy.orm.exc.MultipleResultsFound as e:
        raise ApiException(str(e), status=500)


@app.memberRegistry.endpoint('tag.search', TagSearchValidator)
def tag_search(api_request: TagSearchRequest):
    """
    Action to global tags search 
    :param api_request: 
    :return: 
    """
    query = (Tag.query.filter(Tag.name.ilike('%'+api_request.name+'%')))
    query = query.order_by(Tag.name.desc())
    query = query.limit(api_request.limit)
    tags = query.all()

    models = []
    for tag in tags:
        models.append(ModelBuilder.build_model(tag))

    return ApiResponse(models)


@app.memberRegistry.endpoint('tag.list', TagListValidator)
def tag_list(api_request: TagListRequest):
    """
    Action to list all users tags (attached to topics)
    :param api_request: 
    :return: 
    """
    user = current_user()
    query = Tag.query.join(Topic.tags).filter(Topic.user_id == user.id)
    if api_request.categories:
        query = query.join(Category, Topic.category)\
            .filter(Category.uuid.in_(api_request.categories))\
            .filter(Topic.category_id == Category.id)

    # count topics for tag
    rq = sqlalchemy.select([sqlalchemy.column('tag_id'), sqlalchemy.func.count('tag_id').label('count')])\
        .order_by(sqlalchemy.asc(sqlalchemy.column('tag_id'))).group_by(sqlalchemy.column('tag_id'))\
        .where(Topic.user_id == user.id)\
        .select_from(TopicTags.join(Topic, Topic.id == TopicTags.c.topic_id))

    tags = query.all()
    result = {}
    for row in app.db.session.execute(rq):
        result[row['tag_id']] = row['count']

    for tag in tags:
        tag.count = result.get(tag.id, 0)

    return ApiResponse(ModelBuilder.build_models(tags, groups=['Default', 'List']))


@app.memberRegistry.endpoint('tag.create', TagCreateValidator)
def tag_create(api_request: TagCreateRequest):
    """
    Action to create tag
    :param api_request: 
    :return: 
    """
    tag = Tag()
    tag.name = api_request.name

    app.db.session.add(tag)
    app.db.session.commit()

    return ApiResponse(ModelBuilder.build_model(tag))


@app.memberRegistry.endpoint('tag.update', TagUpdateValidator)
def tag_update(api_request: TagUpdateRequest):
    """
    Action to update tag
    :param api_request: 
    :return: 
    """
    tag = Tag.query.filter(Tag.uuid == uuid.UUID(api_request.uuid))

    if tag is None:
        raise ApiException('Tag not found with uuid "{}"'.format(api_request.uuid), status=404)

    tag.name = api_request.name

    app.db.session.commit()

    return ApiResponse(ModelBuilder.build_model(tag))


@app.memberRegistry.endpoint('category.search', CategorySearchValidator)
def category_search(api_request: CategorySearchRequest):
    """
    Action to search category
    :param api_request: 
    :return: 
    """
    user = current_user()

    query = Category.query\
        .filter(Category.name.ilike('%'+api_request.name+'%'))\
        .filter(Category.parent_id.is_(None))\
        .filter(Category.user_id == user.id)
    query = query.order_by(Category.name.desc())
    query = query.limit(api_request.limit)
    query = query.options(sqlalchemy.orm.lazyload('children'))
    categories = query.all()

    return ApiResponse(ModelBuilder.build_models(categories))


@app.memberRegistry.endpoint('category.list', CategoryListValidator)
def category_list(api_request: CategoryListRequest):
    """
    Action to list categories
    :param api_request: 
    :return: 
    """
    user = current_user()
    query = Category.query.filter(Category.user_id == user.id).filter(Category.parent_id.is_(None))
    # count topics for category
    rq = sqlalchemy.select([sqlalchemy.column('category_id'), sqlalchemy.func.count('category_id').label('count')]) \
        .order_by(sqlalchemy.asc(sqlalchemy.column('category_id'))).group_by(sqlalchemy.column('category_id')) \
        .where(Topic.user_id == user.id) \
        .select_from(Topic)

    categories = query.all()
    result = {}
    for row in app.db.session.execute(rq):
        result[row['category_id']] = row['count']

    for category in categories:
        category.count = result.get(category.id, 0)

    return ApiResponse(ModelBuilder.build_models(categories))


@app.memberRegistry.endpoint('category.create', CategoryCreateValidator)
def category_create(api_request: CategoryCreateRequest):
    """
    Action to create category
    :param api_request: 
    :return: 
    """
    user = current_user()
    category = Category()
    category.name = api_request.name
    category.user_id = user.id
    # if api_request.parent:
    #     parent_category = Category.query.get(api_request.parent)
    #     if parent_category is None:
    #         raise ApiException('Category not found with id "{}"'.format(api_request.parent), status=404)
    #     if parent_category.user_id != user.id:
    #         raise ApiException('Access denied', status=403)
    #     category.parent_id = parent_category.id

    app.db.session.add(category)
    app.db.session.commit()

    return ApiResponse(ModelBuilder.build_model(category))


@app.memberRegistry.endpoint('category.update', CategoryUpdateValidator)
def category_update(api_request: CategoryUpdateRequest):
    """
    Action to update category
    :param api_request: 
    :return: 
    """
    user = current_user()
    category = Category.query.filter(Category.uuid == uuid.UUID(api_request.uuid))

    if category is None:
        raise ApiException('Category not found with uuid "{}"'.format(api_request.uuid), status=404)
    if category.user_id != user.id:
        raise ApiException('Access denied', status=403)

    category.name = api_request.name
    # if api_request.parent and category.parent_id != api_request.parent:
    #     parent_category = Category.query.get(api_request.parent)
    #     if parent_category is None:
    #         raise ApiException('Category not found with id "{}"'.format(api_request.parent), status=404)
    #     if parent_category.user_id != user.id:
    #         raise ApiException('Access denied', status=403)
    #     category.parent_id = parent_category.id

    app.db.session.commit()

    return ApiResponse(ModelBuilder.build_model(category))


@app.memberRegistry.endpoint('category.remove', CategoryActionUUIDValidator)
def topic_delete(api_request: CategoryActionUUIDRequest):
    """
    Action to delete category
    :param api_request: 
    :return: 
    """
    user = current_user()
    category = Category.query.filter(Tag.uuid == api_request.uuid)

    if category is None:
        raise ApiException('Category not found with uuid "{}"'.format(api_request.uuid), status=404)
    if category.user_id != user.id:
        raise ApiException('Access denied', status=403)

    count = Topic.query.filter_by(category_id=category.id).count()

    if count > 0:
        un_categorized = Category.query.filter_by(name=Category.UNCATEGORIZED)\
            .filter_by(user_id=user.id).order_by(Category.id.desc()).first()
        if not un_categorized:
            un_categorized = Category()
            un_categorized.name = Category.UNCATEGORIZED
            un_categorized.user_id = user.id

            app.db.session.add(un_categorized)
            app.db.session.commit()
            app.db.session.refresh(un_categorized)

        Topic.query.filter(Topic.category_id == category.id).update({Topic.category_id: un_categorized.id})

    app.db.session.delete(category)
    app.db.session.commit()

    return ApiResponse()
