import datetime
import os

import flask
import sqlalchemy
import werkzeug.utils

import app.bundle.topics.api
from app import db
from app.bundle.api.authetication.authentication import (current_user, auth_token_required)
from app.bundle.api.components.model_builder.model_builder import ModelBuilder
from app.bundle.api.components.utils import (entity_sequences_diff, allowed_file)
from app.bundle.api.exception.api_exception import (ApiException, ValidationApiException)
from app.bundle.api.server.json_rpc_server import JsonRPCServer
from app.bundle.api.server.response.response import ApiResponse
from app.bundle.topics.models import (Category, Tag, Topic, TopicAttachment)
from app.bundle.topics.request.validator import (
    TopicAttachRestValidator,
    TopicCreateRestValidator,
    TopicUpdateRestValidator,
)

topicBlueprint = flask.Blueprint('topic', __name__)


@topicBlueprint.route('/api/topic/create', methods=['POST'], strict_slashes=False)
@auth_token_required
def topic_create_rest():
    try:
        validator = TopicCreateRestValidator(flask.request)

        if not validator.validate():
            raise ValidationApiException('Request is not valid', status=400, violations=validator.errors)

        topic = _init_topic()
        _update_topic_tags(topic)
        _update_extra_content(topic)
        _update_attachments(topic)
        _update_category(topic)

        db.session.add(topic)
        db.session.commit()

        return JsonRPCServer.handle_api_response(ApiResponse(ModelBuilder.build_model(topic)))
    except Exception as e:
        return JsonRPCServer.handle_api_error(e)


@topicBlueprint.route('/api/topic/<uuidhex:topic_uuid>/update', methods=['POST'], strict_slashes=False)
@auth_token_required
def topic_update_rest(topic_uuid):
    try:
        validator = TopicUpdateRestValidator(flask.request)
        if not validator.validate():
            raise ValidationApiException('Request is not valid', status=400, violations=validator.errors)

        topic = _init_topic(topic_uuid)
        _update_topic_tags(topic)
        _update_extra_content(topic)
        _update_attachments(topic)
        _update_category(topic)

        db.session.commit()

        return JsonRPCServer.handle_api_response(ApiResponse(ModelBuilder.build_model(topic)))
    except Exception as e:
        return JsonRPCServer.handle_api_error(e)


@topicBlueprint.route('/api/topic/<uuidhex:topic_uuid>/attach', methods=['POST'], strict_slashes=False)
@auth_token_required
def topic_attachments(topic_uuid):
    try:
        validator = TopicAttachRestValidator(flask.request)
        if not validator.validate():
            raise ValidationApiException('Request is not valid', status=400, violations=validator.errors)

        user = current_user()
        topic = Topic.query.filter_by(uuid=topic_uuid).one()
        if not Topic:
            raise ApiException('Topic not found with uuid "{}"'.format(topic_uuid), status=404)
        if topic.user_id != user.id:
            raise ApiException('Access denied', status=403)

        attaches =  _update_attachments(topic)

        db.session.commit()

        return JsonRPCServer.handle_api_response(ApiResponse(attaches))
    except Exception as e:
        return JsonRPCServer.handle_api_error(e)


def _init_topic(topic_uuid=None):
    """
    Initialize topic

    :param topic:
    :return:
    """
    user = current_user()

    if topic_uuid:
        topic = Topic.query.filter_by(uuid=topic_uuid).one()

        if not Topic:
            raise ApiException('Topic not found with uuid "{}"'.format(topic_uuid), status=404)
        if topic.user_id != user.id:
            raise ApiException('Access denied', status=403)
    else:
        topic = Topic()

    topic.title = flask.request.form.get('title')
    topic.description = flask.request.form.get('description')
    topic.user_id = user.id
    topic.content = flask.request.form.get('content')
    topic.attachments = []
    topic.extra_content = []
    topic.public = flask.request.form.get('public') or False
    topic.updated_at = datetime.datetime.utcnow()

    return topic


def _update_topic_tags(topic: Topic):
    """
    Updates topic tags

    :param topic:
    :return:
    """
    request_tags = list(map(lambda tg: flask.json.loads(tg), flask.request.form.getlist('tags[]')))

    if request_tags:
        # diff with requested tags and topic tags by id and name
        remove_tags = list(
            entity_sequences_diff(
                list(entity_sequences_diff(topic.tags, request_tags)),
                request_tags,
                'name'
            )
        )

        for tag in remove_tags:
            topic.tags.remove(tag)

        request_tags = list(
            entity_sequences_diff(
                list(entity_sequences_diff(request_tags, topic.tags, 'name')),
                topic.tags
            )
        )

        tags_ids = [tag.get('id') for tag in request_tags if tag.get('id')]
        tags_names = [tag.get('name') for tag in request_tags if not tag.get('id')]
        if tags_ids:
            tags_by_id = Tag.query.filter(Tag.id.in_(tags_ids)).all()
            for tag in tags_by_id:
                topic.tags.append(tag)

        if tags_names:
            tags_by_name = Tag.query.filter(Tag.name.in_(tags_names)).all()
            for tag in tags_by_name:
                tags_names.remove(tag.name)
                topic.tags.append(tag)
            for name in tags_names:
                topic_tag = Tag()
                topic_tag.name = name
                topic.tags.append(topic_tag)
    else:
        topic.tags.clear()


def _update_extra_content(topic: Topic):
    """
    Updates topic extra content

    :param topic:
    :return:
    """
    # request_extra_content = request.form.getlist('extra_content')
    #
    # if request_extra_content:
    #     for position, content in enumerate(request_extra_content):
    #         content = json.loads(content)
    #         topic_content = TopicContent()
    #         topic_content.name = content.get('name')
    #         topic_content.value = content.get('value')
    #         topic_content.type = content.get('type')
    #         topic_content.position = position
    #         topic.content.append(topic_content)
    # else:
    #     topic.extra_content.clear()

    # request_extra_content = request.form.getlist('extra_content')

    # if request_extra_content:
    #     # Process content
    #     for position, content in enumerate(request_extra_content):
    #         content = json.loads(content)
    #         if content.get('id'):
    #             topic_content = next((tc for tc in topic.extra_content if tc.id == content.get('id')), None)
    #         else:
    #             topic_content = TopicContent()
    #             topic.extra_content.append(topic_content)
    #
    #         if topic_content is not None:
    #             topic_content.name = content.get('name')
    #             topic_content.value = content.get('value')
    #             topic_content.type = content.get('type')
    #             topic_content.position = position
    # else:
    #     topic.extra_content.clear()
    pass


def _update_attachments(topic: Topic):
    """
    Updates topic attachments

    :param topic:
    :return list of new attachments:
    """
    attaches = []
    request_attachments = list(map((lambda att: flask.json.loads(att)), flask.request.form.getlist('attachments')))

    if request_attachments:
        attachments_remove = list(entity_sequences_diff(topic.attachments, request_attachments))
        for attach_remove in attachments_remove:
            topic.attachments.remove(attach_remove)
    else:
        topic.attachments.clear()

    if 'attachments' in flask.request.files:
        current_length = len(topic.attachments)

        for key, file in enumerate(flask.request.files.getlist('attachments')):
            if file and file.filename and allowed_file(file.filename):
                filename = werkzeug.utils.secure_filename(file.filename)
                file_path = os.path.join(flask.current_app.config['UPLOAD_FOLDER'], filename)
                file.save(os.path.realpath(flask.current_app.root_path + '/../' + file_path))

                topic_attach = TopicAttachment()
                topic_attach.name = ''
                topic_attach.path = file_path
                topic_attach.position = current_length + key

                topic.attachments.append(topic_attach)
                attaches.append(topic_attach.uuid)

    return attaches


def _update_category(topic: Topic):
    """
    Updates topic category

    :param topic:
    :return:
    """
    user = current_user()

    default_category = Category()
    default_category.name = Category.UNCATEGORIZED
    default_category.user_id = user.id

    if flask.request.form.get('category'):
        request_category = flask.json.loads(flask.request.form.get('category'))

        if request_category.get('id') and topic.validate_category_id(request_category.get('id')):
            category = Category.query.get(request_category.get('id'))
            if category is None:
                raise ApiException('Category not found with id "{}"'.format(request_category.get('id')),
                                   status=404)
            if category.user_id != user.id:
                raise ApiException('Access denied', status=403)
            topic.category = category

            return

        elif request_category.get('name') and topic.validate_category_name(request_category.get('name')):
            default_category.name = request_category.get('name')
            category = Category.query.filter(Category.user_id == user.id) \
                .filter(Category.name == request_category.get('name')).one_or_none()

            topic.category = category if category else default_category

            return

    category = Category.query.filter(
        sqlalchemy.and_(Category.name == Category.UNCATEGORIZED, Category.user_id == user.id)
    ).one_or_none()

    topic.category = category if category else default_category
