import binascii
import datetime
import hashlib
import uuid

import sqlalchemy.dialects

import app
from app.bundle.api.components.model_builder.model_builder import (ModelBuilder)
from app.bundle.api.models import BaseModel
from app.bundle.users.models import (UserGroupsTopics)

TopicTags = app.db.Table(
    'topic_tags',
    app.db.Column('tag_id', app.db.Integer, app.db.ForeignKey('tag.id')),
    app.db.Column('topic_id', app.db.Integer, app.db.ForeignKey('topics.id'))
)


class TopicShareLink(app.db.Model, BaseModel):
    """Topic shared link"""
    __tablename__ = 'topic_share_link'
    __table_args__ = (
        app.db.Index(
            'idx_token',
            'token'
        ),
    )

    token = app.db.Column(app.db.Binary(32), primary_key=True)
    topic_id = app.db.Column(app.db.Integer, app.db.ForeignKey('topics.id', onupdate="NO ACTION", ondelete="CASCADE"))
    created_at = app.db.Column(app.db.DateTime, nullable=False)

    def __init__(self, topic_id):
        self.topic_id = topic_id
        self.created_at = datetime.datetime.utcnow()

    """
    Set share token
    """
    def set_token(self, token):
        self.token = TopicShareLink.token_to_binary(token)

    """
    Get share token
    """
    def get_token(self):
        return binascii.hexlify(self.token).decode('utf-8')

    @staticmethod
    def token_to_binary(token):
        return binascii.unhexlify(token.encode())


class Topic(app.db.Model, BaseModel):
    """Topics model for storing users topics"""
    __tablename__ = 'topics'
    __table_args__ = (
        app.db.Index(
            'idx_topic_search_vector',
            'search_vector',
            postgresql_using='gin'
        ),
        app.db.Index(
            'idx_topic_user',
            'user_id'
        ),
        app.db.Index(
            'idx_topic_uuid',
            'uuid',
            unique=True
        )
    )

    id = app.db.Column(app.db.BigInteger, primary_key=True, autoincrement=True)
    uuid = app.db.Column(sqlalchemy.dialects.postgresql.UUID(as_uuid=True), nullable=False)
    user_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)
    title = app.db.Column(app.db.String(length=255), nullable=True)
    description = app.db.Column(app.db.Text, nullable=True)
    content = app.db.Column(app.db.Text, nullable=False)
    attachments = app.db.relationship('TopicAttachment', backref=app.db.backref('topic', lazy='joined'),
                                  order_by='TopicAttachment.position', cascade="all, delete-orphan")
    extra_content = app.db.relationship('TopicContent', backref=app.db.backref('topic', lazy='joined'),
                                    order_by='TopicContent.position', cascade="all, delete-orphan")
    category_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey('category.id'))
    category = app.db.relationship('Category', backref=app.db.backref('topics', lazy='joined'))
    tags = app.db.relationship('Tag', secondary=TopicTags, backref=app.db.backref('topics', lazy='dynamic'))
    created_at = app.db.Column(app.db.DateTime, nullable=False)
    updated_at = app.db.Column(app.db.DateTime, nullable=False)
    search_vector = app.db.Column(sqlalchemy.dialects.postgresql.TSVECTOR)
    groups = app.db.relationship(
        "Group",
        lazy="dynamic",
        secondary=UserGroupsTopics,
        back_populates="topics"
    )
    shared = app.db.relationship('TopicShareLink', uselist=False, lazy='noload')
    public = app.db.Column(app.db.Boolean, nullable=False)

    def __init__(self):
        self.uuid = uuid.uuid1()
        self.created_at = datetime.datetime.utcnow()

    """
    Validate category by id
    Returns True if category not found and False otherwise
    """
    def validate_category_id(self, id):
        return not self.category or self.category.id != id

    """
    Validate category by name
    Returns True if category not found and False otherwise
    """
    def validate_category_name(self, name):
        return not self.category or self.category.name != name

    def build_hash(self):
        return hashlib.sha256(str(
                str(self.user_id) + str(self.user_id) + str(getattr(self.uuid, 'hex')) + str(self.created_at)
        ).encode('utf-8')).hexdigest()

    @staticmethod
    def build_share_key(share_type, topic_hash):
        return 'topic.' + share_type + '.' + topic_hash

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'user_id': ['Default', 'User'],
            'title': ['Default'],
            'description': ['Default'],
            'content': ['Default'],
            'attachments': ['Default'],
            # 'extra_content': ['Default'],
            'category': ['Default'],
            'tags': ['Default'],
            'created_at': ['Default'],
            'shared': ['Shared'],
            'public': ['Default'],
        }

    def get_transformed(self, groups: list = list()):
        transformed = {
            'uuid': getattr(self.uuid, 'hex'),
            'created_at': str(self.created_at),
            'public': not not self.public,
        # 'extra_content': list(map(lambda content: content.get_model(), self.extra_content)),
        # 'category': ModelBuilder.build_model(self.category, groups=groups),
        # 'attachments': list(map(lambda attach: ModelBuilder.build_model(attach, groups=groups), self.attachments)),
        # 'tags': list(map(lambda tag: ModelBuilder.build_model(tag, groups=groups), self.tags)),
        }

        if list(set(groups).intersection(self.get_normalized().get('shared'))) and (not not self.shared):
            transformed['shared'] = getattr(self.shared, 'get_token')()

        if list(set(groups).intersection(self.get_normalized().get('attachments'))):
            transformed['attachments'] = list(
                map(lambda attach: ModelBuilder.build_model(attach, groups=groups), self.attachments))

        if list(set(groups).intersection(self.get_normalized().get('tags'))):
            transformed['tags'] = list(map(lambda tag: ModelBuilder.build_model(tag, groups=groups), self.tags))

        return transformed


class TopicAttachment(app.db.Model, BaseModel):
    """Topics content"""
    __tablename__ = 'topics_attach'
    __table_args__ = (
        app.db.Index(
            'idx_topic_attach_topic',
            'topic_id'
        ),
        app.db.Index(
            'idx_topic_attach_uuid',
            'uuid',
            unique=True
        )
    )

    id = app.db.Column(app.db.BigInteger, primary_key=True, autoincrement=True)
    uuid = app.db.Column(sqlalchemy.dialects.postgresql.UUID(as_uuid=True), nullable=False)
    topic_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey("topics.id", ondelete='CASCADE'), nullable=False)
    name = app.db.Column(app.db.String(length=255), nullable=True)
    path = app.db.Column(app.db.String(length=255), nullable=True)
    position = app.db.Column(app.db.SmallInteger(),  nullable=False)
    created_at = app.db.Column(app.db.DateTime, nullable=False)

    def __init__(self):
        self.uuid = uuid.uuid1()
        self.created_at = datetime.datetime.utcnow()

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'name': ['Default'],
            'topic_id': ['Default'],
            'path': ['Default'],
            'created_at': ['Default'],
            'position': ['Default'],
        }

    def get_transformed(self, groups: list = list()):
        return {
            'uuid': getattr(self.uuid, 'hex'),
            'created_at': str(self.created_at),
        }


class TopicContent(app.db.Model, BaseModel):
    """Topics content"""
    __tablename__ = 'topics_content'
    __table_args__ = (
        app.db.Index(
            'idx_topic_content_search_vector',
            'search_vector',
            postgresql_using='gin'
        ),
        app.db.Index(
            'idx_topic_extra_topic',
            'topic_id'
        ),
        app.db.Index(
            'idx_topic_extra_uuid',
            'uuid',
            unique=True
        )
    )

    id = app.db.Column(app.db.BigInteger, primary_key=True, autoincrement=True)
    uuid = app.db.Column(sqlalchemy.dialects.postgresql.UUID(as_uuid=True), nullable=False)
    topic_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey("topics.id", ondelete='CASCADE'), nullable=False)
    name = app.db.Column(app.db.String(length=255), nullable=True)
    value = app.db.Column(app.db.Text, nullable=False)
    type = app.db.Column(app.db.String(length=16),  nullable=True)
    position = app.db.Column(app.db.SmallInteger(),  nullable=False)
    search_vector = app.db.Column(sqlalchemy.dialects.postgresql.TSVECTOR)

    def __init__(self):
        self.uuid = uuid.uuid1()

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'name': ['Default'],
            'topic_id': ['Default'],
            'value': ['Default'],
            'type': ['Default'],
            'position': ['Default'],
        }

    def get_transformed(self, groups: list = list()):
        return {
            'uuid': getattr(self.uuid, 'hex'),
        }


class Category(app.db.Model, BaseModel):
    UNCATEGORIZED = 'Uncategorized'
    """Category model to store categories info."""
    __tablename__ = "category"
    __table_args__ = (
        app.db.Index(
            'idx_category_user',
            'user_id'
        ),
        app.db.Index(
            'idx_category_uuid',
            'uuid',
            unique=True
        )
    )

    id = app.db.Column(app.db.BigInteger, primary_key=True, autoincrement=True)
    uuid = app.db.Column(sqlalchemy.dialects.postgresql.UUID(as_uuid=True), nullable=False)
    name = app.db.Column(app.db.String(length=255), nullable=False)
    parent_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey('category.id', ondelete="SET NULL"))
    children = app.db.relationship('Category', backref=app.db.backref('parent', remote_side=[id]), lazy='joined')
    user_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)
    count = 0 # topic count

    def __init__(self):
        self.uuid = uuid.uuid1()

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'name': ['Default'],
            # 'parent_id': ['List', 'Default'],
            # 'children': ['List'],
            'user_id': ['User'],
            'count': ['List']
        }

    def get_transformed(self, groups: list = list()):
        return {
            'uuid': getattr(self.uuid, 'hex'),
        }


class Tag(app.db.Model, BaseModel):
    """Tags table to store topics tags."""
    __tablename__ = 'tag'
    __table_args__ = (
        app.db.Index(
            'idx_tag_name',
            'name',
            unique=True
        ),
        app.db.Index(
            'idx_tag_uuid',
            'uuid',
            unique=True
        ),
    )

    id = app.db.Column(app.db.BigInteger, primary_key=True, autoincrement=True)
    uuid = app.db.Column(sqlalchemy.dialects.postgresql.UUID(as_uuid=True), nullable=False)
    name = app.db.Column(app.db.String(length=64), nullable=False, unique=True)
    count = 0 # topic count

    def __init__(self):
        self.uuid = uuid.uuid1()

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'name': ['Default'],
            'count': ['List']
        }

    def get_transformed(self, groups: list = list()):
        return {
            'uuid': getattr(self.uuid, 'hex'),
        }


class TopicsUsers(app.db.Model, BaseModel):
    """Topics users association table."""
    __tablename__ = 'topics_users'

    topic_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey('topics.id'), primary_key=True)
    topic = app.db.relationship('Topic', backref=app.db.backref('users', lazy='dynamic'))
    user_id = app.db.Column(app.db.BigInteger, app.db.ForeignKey('users.id'), primary_key=True)
    user = app.db.relationship('User', back_populates='topics', lazy='joined')
    permission = app.db.Column(app.db.Integer, nullable=False)
