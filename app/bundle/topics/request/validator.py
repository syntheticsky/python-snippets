from wtforms.validators import (
    DataRequired,
    Length,
    NumberRange,
    Optional,
    Regexp,
)

from app.bundle.api.components.validator.constraints import (
    All,
    IsTypeValidator,
    JsonSchema,
    JsonValidator,
    RegexpValidator
)
from app.bundle.api.components.validator.validator import BaseActionValidator, Validator

tag_category_schema = {
    "type": "object",
    "properties": {
        "id": {"type": ["null", "integer"]},
        "name": {"type": "string"}
    }
}


class TopicActionValidator(BaseActionValidator):
    parameters = {
        'id': [DataRequired(), Regexp(r'^\d+$', message='Only digits allowed')],
    }


class TopicActionUuidValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
    }


class TopicListValidator(BaseActionValidator):
    parameters = {
        'page': [NumberRange(min=0)],
        'limit': [NumberRange(min=1, max=50)],
        'tags': [Optional(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])],
        'categories': [Optional(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])]
    }


class TopicSearchValidator(BaseActionValidator):
    parameters = {
        'query': [Length(min=2)],
        'tags': [Optional(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])],
        'categories': [Optional(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])]
    }


class TopicCreateRestValidator(Validator):
    form = {
        'title': [Optional()],
        'description': [Optional()],
        'content': [DataRequired()],
        'extra_content': [Optional()],
        'tags[]': [Optional(), All([JsonValidator(schema=tag_category_schema)])],
        'category': [Optional(), JsonSchema(schema=tag_category_schema)],
        'public': [Optional(), IsTypeValidator(var_type=bool)],
    }


class TopicUpdateRestValidator(Validator):
    form = {
        'title': [Optional()],
        'description': [Optional()],
        'content': [DataRequired()],
        'extra_content': [Optional()],
        'tags[]': [Optional(), All([JsonValidator(schema=tag_category_schema)])],
        'category': [Optional(), JsonSchema(schema=tag_category_schema)],
        'public': [Optional(), IsTypeValidator(var_type=bool)],
    }


class TopicAttachRestValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
    }


class TopicCreateValidator(BaseActionValidator):
    parameters = {
        'title': [Optional()],
        'description': [Optional()],
        'content': [DataRequired()],
        'extra_content': [Optional()],
        'tags': [Optional()],
        'public': [Optional(), IsTypeValidator(var_type=bool)]
    }


class TopicUpdateValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
        'title': [Optional()],
        'description': [Optional()],
        'content': [DataRequired()],
        'extra_content': [Optional()],
        'tags': [Optional(), All([JsonValidator(schema=tag_category_schema)])],
        'category': [Optional(), JsonSchema(schema=tag_category_schema)],
        'public': [Optional(), IsTypeValidator(var_type=bool)]
    }


class TopicActionSharedValidator(BaseActionValidator):
    parameters = {
        'token': [DataRequired(), Regexp(r'^[0-9a-fA-F]{64}$', message='Wrong shared token')],
    }


class TagActionValidator(BaseActionValidator):
    parameters = {
        'id': [DataRequired(), Regexp(r'\d+')],
    }


class TagCreateValidator(BaseActionValidator):
    parameters = {
        'name': [DataRequired()],
    }


class TagUpdateValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
        'name': [DataRequired()],
    }


class TagListValidator(BaseActionValidator):
    parameters = {
        # 'page': [NumberRange(min=0)],
        # 'limit': [NumberRange(min=1, max=50)],
        'categories': [Optional(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])]
    }


class TagSearchValidator(BaseActionValidator):
    parameters = {
        'name': [DataRequired()],
        'limit': [NumberRange(min=1)],
    }


class CategoryActionValidator(BaseActionValidator):
    parameters = {
        'id': [DataRequired(), Regexp(r'\d+')],
    }


class CategoryActionUUIDValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
    }


class CategoryCreateValidator(BaseActionValidator):
    parameters = {
        'name': [DataRequired()],
        'parent': [Optional(), Regexp(r'\d+')],
    }


class CategoryUpdateValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
        'name': [DataRequired()],
        'parent': [],
    }


class CategoryListValidator(BaseActionValidator):
    parameters = {
        'page': [NumberRange(min=0)],
        'limit': [NumberRange(min=1, max=50)],
    }


class CategorySearchValidator(BaseActionValidator):
    parameters = {
        'name': [DataRequired()],
        'limit': [NumberRange(min=1)],
    }


class HistoryViewValidator(BaseActionValidator):
    parameters = {
        'page': [NumberRange(min=0)],
        'limit': [NumberRange(min=1, max=50)],
        'uuid': [DataRequired(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])],
    }
