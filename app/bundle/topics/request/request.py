from app.bundle.api.server.request.request import (BaseApiRequest, PaginatedApiRequest)
import app.bundle.core.mixins as core_mixins


class TopicActionSharedRequest(BaseApiRequest):
    def __init__(self):
        super(TopicActionSharedRequest, self).__init__()
        self.__token = None

    @property
    def token(self):
        return self.__token

    @token.setter
    def token(self, value):
        self.__token = str(value)

    @token.deleter
    def token(self):
        del self.__token


class TopicActionRequest(BaseApiRequest):
    def __init__(self):
        super(TopicActionRequest, self).__init__()
        self.__id = None

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = str(value)

    @id.deleter
    def id(self):
        del self.__id


class TopicActionUUIDRequest(BaseApiRequest, core_mixins.UniquenessKey):
    def __init__(self):
        super(TopicActionUUIDRequest, self).__init__()
        self.__uuid = None

    @property
    def uuid(self):
        return self.__uuid

    @uuid.setter
    def uuid(self, value):
        self.__uuid = str(value)

    @uuid.deleter
    def uuid(self):
        del self.__uuid

    def get_unique_key(self):
        return self.uuid


class TopicListRequest(PaginatedApiRequest):
    def __init__(self):
        super(TopicListRequest, self).__init__()
        self.__tags = list()
        self.__categories = list()

    @property
    def tags(self):
        return self.__tags

    @tags.setter
    def tags(self, value):
        self.__tags = value

    @tags.deleter
    def tags(self):
        del self.__tags

    @property
    def categories(self):
        return self.__categories

    @categories.setter
    def categories(self, value):
        self.__categories = value

    @categories.deleter
    def categories(self):
        del self.__categories


class TopicSearchRequest(PaginatedApiRequest):
    def __init__(self):
        super(TopicSearchRequest, self).__init__()
        self.__query = ''
        self.__tags = list()
        self.__categories = list()

    @property
    def query(self):
        return self.__query

    @query.setter
    def query(self, value):
        self.__query = value

    @query.deleter
    def query(self):
        del self.__query

    @property
    def tags(self):
        return self.__tags

    @tags.setter
    def tags(self, value):
        self.__tags = value

    @tags.deleter
    def tags(self):
        del self.__tags

    @property
    def categories(self):
        return self.__categories

    @categories.setter
    def categories(self, value):
        self.__categories = value

    @categories.deleter
    def categories(self):
        del self.__categories


class TopicCreateRequest(BaseApiRequest):
    def __init__(self):
        super(TopicCreateRequest, self).__init__()
        self.__title = None
        self.__description = None
        self.__content = None
        self.__extra_content = {}
        self.__tags = {}
        self.__category = {}
        self.__public = False

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, value):
        self.__title = value

    @title.deleter
    def title(self):
        del self.__title

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, value):
        self.__description = value

    @description.deleter
    def description(self):
        del self.__description

    @property
    def content(self):
        return self.__content

    @content.setter
    def content(self, value):
        self.__content = value

    @content.deleter
    def content(self):
        del self.__content

    @property
    def extra_content(self):
        return self.__extra_content

    @extra_content.setter
    def extra_content(self, value):
        self.__extra_content = value

    @extra_content.deleter
    def extra_content(self):
        del self.__extra_content

    @property
    def tags(self):
        return self.__tags

    @tags.setter
    def tags(self, value):
        self.__tags = value

    @tags.deleter
    def tags(self):
        del self.__tags

    @property
    def category(self):
        return self.__category

    @category.setter
    def category(self, value):
        self.__category = value

    @category.deleter
    def category(self):
        del self.__category

    @property
    def public(self):
        return self.__public

    @public.setter
    def public(self, value):
        self.__public = value

    @public.deleter
    def public(self):
        del self.__public


class TopicUpdateRequest(TopicActionUUIDRequest, TopicCreateRequest):
    def __init__(self):
        TopicActionUUIDRequest.__init__(self)
        TopicCreateRequest.__init__(self)


class TagActionRequest(BaseApiRequest):
    def __init__(self):
        super(TagActionRequest, self).__init__()
        self.__id = None

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = str(value)

    @id.deleter
    def id(self):
        del self.__id


class TagActionUUIDRequest(BaseApiRequest):
    def __init__(self):
        super(TagActionUUIDRequest, self).__init__()
        self.__uuid = None

    @property
    def uuid(self) -> int:
        return self.__uuid

    @uuid.setter
    def uuid(self, value):
        self.__uuid = str(value)

    @uuid.deleter
    def uuid(self):
        del self.__uuid


class TagListRequest(BaseApiRequest):
    def __init__(self):
        super(TagListRequest, self).__init__()
        self.__categories = list()

    @property
    def categories(self):
        return self.__categories

    @categories.setter
    def categories(self, value: list):
        self.__categories = value

    @categories.deleter
    def categories(self):
        del self.__categories


class TagCreateRequest(BaseApiRequest):
    def __init__(self):
        super(TagCreateRequest, self).__init__()
        self.__name = None

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @name.deleter
    def name(self):
        del self.__name


class TagSearchRequest(TagCreateRequest):
    def __init__(self):
        super(TagSearchRequest, self).__init__()
        self.__limit = 10

    @property
    def limit(self):
        return self.__limit

    @limit.setter
    def limit(self, value):
        self.__limit = value

    @limit.deleter
    def limit(self):
        del self.__limit


class TagUpdateRequest(TagActionUUIDRequest, TagCreateRequest):
    def __init__(self):
        TagActionUUIDRequest.__init__(self)
        TagCreateRequest.__init__(self)


class CategoryActionRequest(BaseApiRequest):
    def __init__(self):
        super(CategoryActionRequest, self).__init__()
        self.__id = None

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = str(value)

    @id.deleter
    def id(self):
        del self.__id


class CategoryActionUUIDRequest(BaseApiRequest):
    def __init__(self):
        super(CategoryActionUUIDRequest, self).__init__()
        self.__uuid = None

    @property
    def uuid(self) -> int:
        return self.__uuid

    @uuid.setter
    def uuid(self, value):
        self.__uuid = str(value)

    @uuid.deleter
    def uuid(self):
        del self.__uuid


class CategoryListRequest(PaginatedApiRequest):
    def __init__(self):
        super(CategoryListRequest, self).__init__()
        self.__limit = 50


class CategoryCreateRequest(BaseApiRequest):
    def __init__(self):
        super(CategoryCreateRequest, self).__init__()
        self.__name = None
        # self.__parent = None

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @name.deleter
    def name(self):
        del self.__name

    # @property
    # def parent(self):
    #     return self.__parent
    #
    # @parent.setter
    # def parent(self, value):
    #     self.__parent = str(value)
    #
    # @parent.deleter
    # def parent(self):
    #     del self.__parent


class CategorySearchRequest(CategoryCreateRequest):
    def __init__(self):
        super(CategorySearchRequest, self).__init__()
        self.__limit = 10

    @property
    def limit(self):
        return self.__limit

    @limit.setter
    def limit(self, value):
        self.__limit = value

    @limit.deleter
    def limit(self):
        del self.__limit


class CategoryUpdateRequest(CategoryActionUUIDRequest, CategoryCreateRequest):
    def __init__(self):
        CategoryActionUUIDRequest.__init__(self)
        CategoryCreateRequest.__init__(self)


class HistoryViewRequest(PaginatedApiRequest):
    def __init__(self):
        super(HistoryViewRequest, self).__init__()
        self.__uuid = list()
        self.__tags = list()
        self.__categories = list()

    @property
    def uuid(self):
        return self.__uuid

    @uuid.setter
    def uuid(self, value):
        self.__uuid = value

    @uuid.deleter
    def uuid(self):
        del self.__uuid
