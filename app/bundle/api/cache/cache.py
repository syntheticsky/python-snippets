from functools import wraps

import flask

from app import cache
from app.bundle.api.authetication.authentication import current_user
from app.bundle.api.exception.auth_exception import AuthenticationError
import app.bundle.api.server.request.request as action_request
from app.bundle.core.mixins import UniquenessKey


def cached_action(timeout=5 * 60):
    """
    Should be applied as first decorator on action
    :param timeout:
    :return:
    """
    def decorator(f):
        @wraps(f)
        def decorated_fn(*args, **kwargs):
            request = next(iter(args or []), None)

            if request is None:
                raise Exception('Action request is empty')
            if not isinstance(request, action_request.BaseApiRequest):
                raise Exception('{} is only allowed to cache action'.format(action_request.BaseApiRequest.__name__))

            cache_key = ''
            params = ()

            try:
                user = current_user()
                cache_key = 'u/{}/'
                params += (user.id, )
            except AuthenticationError:
                pass

            if isinstance(request, UniquenessKey):
                cache_key += 'uk/{}/'
                params += (request.get_unique_key(), )

            cache_key += 'v/{}/a/{}'
            path = flask.request.path.strip('/').replace('/', '_')
            params = params + (path, request.action_name.replace('.', '_'))

            if isinstance(request, action_request.PaginatedApiRequest):
                cache_key += '/p/{}/l/{}'
                params += (request.page, request.limit, )

            cache_key = cache_key.format(*params)
            rv = cache.get(cache_key)

            if rv is not None:
                return rv

            rv = f(*args, **kwargs)
            cache.set(cache_key, rv, timeout=timeout)

            return rv
        return decorated_fn
    return decorator
