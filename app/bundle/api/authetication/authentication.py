from functools import wraps
from flask import (
    request,
    current_app,
    has_request_context,
    _request_ctx_stack
)

from app.bundle.api.exception.auth_exception import (AuthenticationError)
from sqlalchemy import or_
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import (MultipleResultsFound, NoResultFound)
from wtforms.validators import (
    ValidationError
)
from app import db
from app.bundle.api.exception.api_exception import (ApiException)
from app.bundle.api.server.json_rpc_server import JsonRPCServer
from app.bundle.users.models import User


def auth_token_required(fn):
    """Decorator that protects endpoints using token authentication. The token
    should be added to the request by the client by using a query string
    variable with a name equal to the configuration value of
    `SECURITY_TOKEN_AUTHENTICATION_KEY` or in a request header named that of
    the configuration value of `SECURITY_TOKEN_AUTHENTICATION_HEADER`
    """
    @wraps(fn)
    def decorated(*args, **kwargs):
        if _check_token() or request.method == 'OPTIONS':
            return fn(*args, **kwargs)
        else:
            return _get_unauthorized_response()
    return decorated


def current_user():
    """ Get user from request """
    try:
        user = getattr(_request_ctx_stack.top, 'user')
    except AttributeError as e:
        raise AuthenticationError('Unauthorized error')

    return user


def _check_token():
    logged_user = None
    auth_token = request.headers.get(current_app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'])
    try:
        resp = User.decode_auth_token(auth_token)
    except Exception as e:
        db.session.rollback()
        return False
    if not isinstance(resp, str):
        logged_user = User.query.get(resp)
        if logged_user and has_request_context() and not hasattr(_request_ctx_stack.top, 'user'):
            _request_ctx_stack.top.user = logged_user
    return logged_user is not None


def _get_unauthorized_response():
    return JsonRPCServer.handle_api_error(ApiException('Provide a valid auth token.', status=401))


def is_user_exists_by_email_or_username(form, field):
    try:
        if 'email' not in form.errors and not User.query.filter(
                or_(User.email == field.data, User.username == field.data)
        ).one():
            ex = ValidationError('User does not exist.')
            form.errors.setdefault(field.name, []).append(str(ex))
            raise ex
    except SQLAlchemyError as e:
        if isinstance(e, NoResultFound):
            ex = ValidationError('User does not exist.')
            form.errors.setdefault(field.name, []).append(str(ex))
            raise ex
        else:
            raise e


def is_user_not_exists_by_email_or_username(form, field):
    try:
        if ('username' not in form.errors or 'email' not in form.errors) and User.query.filter(
                or_(User.email == field.data, User.username == field.data)
        ).one():
            ex = ValidationError('User has already exist.')
            form.errors.setdefault(field.name, []).append(str(ex))
            raise ex
    except SQLAlchemyError as e:
        if isinstance(e, NoResultFound):
            return
        elif isinstance(e, MultipleResultsFound):
            ex = ValidationError('User has already exist.')
            form.errors.setdefault(field.name, []).append(str(ex))
            raise ex
        else:
            raise e
