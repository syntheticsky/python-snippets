import app
from app.bundle.api.server.request.request import BaseApiRequest
from app.bundle.api.server.response.response import ApiResponse
from app.bundle.api.cache.cache import cached_action


@app.authRegistry.endpoint('ping')
@app.publicRegistry.endpoint('ping')
@app.memberRegistry.endpoint('ping')
@cached_action()
def ping(api_request: BaseApiRequest):
    """ Ping method to test API
    :param api_request:
    :return ApiResponse:
    """

    return ApiResponse(data='pong')
