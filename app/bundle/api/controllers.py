import flask

import app
import app.bundle.api.api
from app.bundle.api.authetication.authentication import auth_token_required
from app.bundle.api.server.json_rpc_server import JsonRPCServer

apiBlueprint = flask.Blueprint('api', __name__, url_prefix='/api')

def log_error(*args, **kwargs):
    flask.current_app.logger.error(*args, **kwargs)


@apiBlueprint.route('/public/', methods=['GET', 'POST', 'OPTIONS'], strict_slashes=False)
def public():
    server = JsonRPCServer(actions=app.publicRegistry)

    return server.handle(flask.request)


@apiBlueprint.route('/', methods=['GET', 'POST', 'OPTIONS'], strict_slashes=False)
@auth_token_required
def member():
    server = JsonRPCServer(actions=app.memberRegistry)

    return server.handle(flask.request)


@apiBlueprint.route('/auth/', methods=['GET', 'POST', 'OPTIONS'], strict_slashes=False)
def auth():
    server = JsonRPCServer(actions=app.authRegistry)

    return server.handle(flask.request)
