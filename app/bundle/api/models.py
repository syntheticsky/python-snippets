# from sqlalchemy import event
# from flask_sqlalchemy import Model
# from App.db import db
# import uuid;


class BaseModel(object):

    def get_normalized(self):
        return {}

    def get_transformed(self, groups: list = list()):
        return {}

    def group_intersect(self, field, groups):
        return list(set(groups).intersection(self.get_normalized().get(field)))

# class Draft(db.Model):
#     __tablename__ = 'draft'
#
#     id = db.Column(db.String, primary_key=True)
#     name = db.Column(db.String(1000), nullable=False, unique=True)
#     slug = db.Column(db.String(1000))
#     content = db.Column(db.String(5000))
#
#     comments = db.relationship('Comment', backref='entity')
#
#     def __str__(self):
#         return self.name
#
#
# @event.listens_for(Draft, 'after_delete')
# def event_after_delete(mapper, connection, target):
#     # Здесь будет очень важная бизнес логика
#     # Или нет. На самом деле, старайтесь использовать сигналы только
#     # тогда, когда других, более правильных вариантов не осталось.
#     pass
