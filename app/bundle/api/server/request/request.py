class BaseApiRequest:
    """
    Base request class for api actions as argument.
    """
    def __init__(self):
        self.__action_name = None

    @property
    def action_name(self):
        return self.__action_name

    @action_name.setter
    def action_name(self, value):
        self.__action_name = value

    @action_name.deleter
    def action_name(self):
        del self.__action_name
# for var_name in dir(var):
#         if not var_name.startswith('_'):
#             param = {
#                 'name': var_name,
#                 'type': var_name.__class__.__name__,
#             }


class PaginatedApiRequest(BaseApiRequest):
    """
    Base class for paginated request
    """
    def __init__(self):
        super(PaginatedApiRequest, self).__init__()
        self.__page = 0
        self.__limit = 30

    @property
    def page(self):
        return self.__page

    @page.setter
    def page(self, value):
        self.__page = value

    @page.deleter
    def page(self):
        del self.__page

    @property
    def limit(self):
        return self.__limit

    @limit.setter
    def limit(self, value):
        self.__limit = value

    @limit.deleter
    def limit(self):
        del self.__limit
