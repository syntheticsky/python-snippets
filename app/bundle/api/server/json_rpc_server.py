import inspect
import json
import traceback
from time import strftime

from flask import (
    request,
    Response,
    current_app,
)

from app.bundle.api.action.action_registry import ActionRegistry
from app.bundle.api.components.mapper.mapper import Mapper
from app.bundle.api.exception.api_exception import ApiException, ValidationApiException
from app.bundle.api.exception.auth_exception import (AuthenticationError)
from app.bundle.api.server.doc_generator.doc_generator import DocGenerator
from app.bundle.api.server.response.response import ApiResponse


class JsonRPCServer:
    def __init__(self, actions: ActionRegistry):
        self.__actions = actions

    @property
    def actions(self):
        return self.__actions

    def handle(self, _request):
        if _request.method == 'OPTIONS':
            headers = [('Allow', 'GET, POST, HEAD, OPTIONS'), ('Content-Type', 'text/plain')]

            return JsonRPCServer.return_response(response='', status=200, headers=headers)
        elif _request.method == 'GET':
            return JsonRPCServer.doc_generator(self.actions.action_registry)
        elif _request.method == 'POST':
            try:
                body = json.loads(str(_request.data, 'utf-8'))

                if 'method' not in body:
                    raise ApiException('Method not found in request', status=400)

                method = body['method']

                if method not in self.actions.action_registry:
                    raise ApiException('Action function not found', status=400)

                if 'params' not in body:
                    body['params'] = {}

                body['params']['action_name'] = method
                action, validator_class = self.actions.action_registry[method]
                params = inspect.signature(action).parameters

                if 'api_request' in params:
                    api_request = params.get('api_request').annotation()
                    Mapper.map(api_request, body['params'])

                    if validator_class is not None:
                        inputs = validator_class(_request, object=api_request)

                        if not inputs.validate():
                            raise ValidationApiException('Request is not valid', status=400, violations=inputs.errors)

                    response = action(api_request)

                    return JsonRPCServer.handle_api_response(response)
                else:
                    raise Exception('No request found in action')
            except Exception as e:
                JsonRPCServer.log_error()
                return JsonRPCServer.handle_api_error(e)

    @staticmethod
    def log_error():
        tb = traceback.format_exc()
        timestamp = strftime('[%Y-%b-%d %H:%M]')
        current_app.logger.error('%s %s %s %s %s 5xx INTERNAL SERVER ERROR\n%s', timestamp, request.remote_addr,
                                 request.method, request.scheme, request.path, tb)

    @staticmethod
    def handle_api_response(response):
        headers = None
        response_object = {
            'status': 'success',
            'code': 200,
        }

        if isinstance(response, Response):
            return response
        elif isinstance(response, ApiResponse):
            response_object['code'] = response.status
            if response.data is not None:
                response_object['data'] = response.data
            if response.message is not None:
                response_object['message'] = response.message
            headers = response.headers
        elif isinstance(response, (list, dict)):
            response_object['data'] = response

        return JsonRPCServer.return_response(response=json.dumps(response_object), headers=headers)

    @staticmethod
    def handle_api_error(exception: Exception):
        status=500
        response_object = {
            'status': 'fail',
            'code': 500,
            'message': 'Error',
        }

        if isinstance(exception, AuthenticationError):
            exception = ApiException(str(exception), status=401)
        elif isinstance(exception, json.JSONDecodeError):
            response_object['message'] = 'JSON decoder error: ' + str(exception)

        if isinstance(exception, ApiException):
            status=exception.status
            response_object['message'] = str(exception)
            response_object['code'] = exception.status
            if isinstance(exception, ValidationApiException):
                response_object['error'] = exception.violations

        return JsonRPCServer.return_response(response=json.dumps(response_object), status=status)

    @staticmethod
    def doc_generator(api_registry: dict):
        try:
            result = DocGenerator.generate(api_registry)
            return JsonRPCServer.handle_api_response(ApiResponse(data=result, status=200))
        except Exception as e:
            return JsonRPCServer.handle_api_error(ApiException(str(e)))

    @staticmethod
    def return_response(response=None, status=None, headers=None, mimetype='application/json', content_type=None,
                        direct_passthrough=False):
        return Response(response=response, mimetype=mimetype, status=status, headers=headers, content_type=content_type,
                        direct_passthrough=direct_passthrough)
