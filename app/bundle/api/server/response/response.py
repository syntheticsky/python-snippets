from werkzeug.datastructures import Headers


class ApiResponse:
    def __init__(self, data=None, status=200, headers: list=None, message=None):
        self.data = data
        self.status = status
        self.headers = Headers(headers) if headers else Headers()
        self.message = message
