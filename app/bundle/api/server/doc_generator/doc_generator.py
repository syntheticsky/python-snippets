import inspect
from typing import Callable

from app.bundle.api.server.request.request import BaseApiRequest


class DocGenerator(object):
    @staticmethod
    def generate(action_registry: dict) -> dict:
        docs = {}
        for name, action_tuple in action_registry.items():
            action, validator_class = action_tuple

            if hasattr(action, '__wrapped__'):
                action = action.__wrapped__

            docs[name] = {}
            docs[name]['description'] = action.__doc__
            docs[name]['as_string'] = inspect.formatargspec(*inspect.getfullargspec(action))
            docs[name]['params'] = DocGenerator.params_doc_generate(action)
        return docs

    @staticmethod
    def params_doc_generate(func: Callable[[], dict]):
        params_list = []
        annotation = inspect.getfullargspec(func).annotations
        for name, var in annotation.items():
            if issubclass(var, BaseApiRequest):
                for var_name in dir(var):
                    if not var_name.startswith('_') and var_name != 'action_name':
                        param = {
                            'name': var_name,
                            'type': var_name.__class__.__name__,
                        }

                        params_list.append(param)

        return params_list
