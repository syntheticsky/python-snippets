from app.bundle.api.components.validator.validator import Validator


class ActionRegistry:
    def __init__(self):
        self.action_registry = {}

    def endpoint(self, endpoint: str, validator: Validator = None):
        """ Store api methods
        """
        def decorator(f):
            self.action_registry[endpoint] = (f, validator)
            return f
        return decorator
