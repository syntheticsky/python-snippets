from flask_sqlalchemy import Model
from flask import (
    current_app,
)


def entity_sequences_intersect(seq: list, seq_comp: list, field='id'):
    for el in seq:
        base_value = getattr(el, field) if isinstance(el, Model) else el.get(field)
        for comp in seq_comp:
            check_value = comp.get(field) if isinstance(comp, dict) else getattr(comp, field)
            if base_value == check_value:
                yield el
                break


def entity_sequences_diff(seq: list, seq_comp: list, field='id'):
    for el in seq:
        found = False
        base_value = getattr(el, field) if isinstance(el, Model) else el.get(field)
        for comp in seq_comp:
            check_value = comp.get(field) if isinstance(comp, dict) else getattr(comp, field)
            if base_value == check_value:
                found = True
                break
        if not found:
            yield el


def allowed_file(filename):
    return ('.' in filename and
            filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS'])
