from flask_sqlalchemy import Model
from flask import (
    current_app,
)
from app.bundle.api.models import BaseModel


class ModelBuilder(object):
    """
    Helper class to build models from db.Model entities inherited from BaseModel
    """
    @staticmethod
    def build_model(entity: BaseModel, groups: list = list()):
        model = {}

        if not entity:
            return

        if not groups:
            groups = ['Default']

        transformed = entity.get_transformed(groups=groups)

        for attr, norm_groups in entity.get_normalized().items():
            for group in iter(groups):
                if group in norm_groups and attr not in model:
                    if transformed.get(attr, None) is not None:
                        value = transformed[attr]
                    else:
                        value = getattr(entity, attr)
                    if isinstance(value, BaseModel):
                        value = ModelBuilder.build_model(value, groups)
                    # elif isinstance(value, (list, )):
                    #     value = list(map(lambda child: ModelBuilder.build_model(child, groups=groups), value))
                    model[attr] = value
        return model

    @staticmethod
    def build_models(entities: list, groups: list = list()):
        return [ModelBuilder.build_model(entity, groups=groups) for entity in entities]


def entity_sequences_diff(seq: list, seq_comp: list, field='id'):
    for el in seq:
        found = False
        base_value = getattr(el, field) if isinstance(el, Model) else el.get('id')
        for comp in seq_comp:
            check_value = comp.get('id') if isinstance(comp, dict) else getattr(comp, field)
            if base_value == check_value:
                found = True
        if not found:
            yield el


def allowed_file(filename):
    return ('.' in filename and
            filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS'])
