from sqlalchemy.orm import (query)
from app.bundle.api.components.model_builder.model_builder import ModelBuilder


class Pagination(object):

    def __init__(self, q: query.Query, page, limit):
        if not isinstance(q, query.Query):
            raise TypeError("Pagination query should be type of SQLAlchemy Query but {} is given".format(type(q)))

        self.page = page
        self.limit = limit
        self.query = q
        self.count = None
        self.storage = []

    def init(self):
        self.count = self.query.count()
        self.query = self.query.offset(self.page*self.limit)
        self.query = self.query.limit(self.limit)
        self.storage.extend(self.query.all())

    def get_model(self, groups: list = list()):
        self.init()
        models = []

        for item in self.storage:
            models.append(ModelBuilder.build_model(item, groups=groups))

        return {
            'page': self.page,
            'limit': self.limit,
            'total': self.count,
            'storage': models,
        }