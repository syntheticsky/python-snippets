import json
import jsonschema
import re
from wtforms.validators import (ValidationError, DataRequired,)
from wtforms.compat import string_types


class JsonSchema(object):
    def __init__(self, schema, message=None):
        """Helper class for JSON validation using jsonschema.
        :param schema: JSON schema to validate against.
        :param message: Error message to return. Defaults to jsonschema's errors.
        :raises: wtforms.validators.ValidationError
        """
        self.schema = schema
        self.message = message

    def __call__(self, form, field):
        try:
            data = json.loads(field.data)
            jsonschema.validate(data, self.schema)
        except TypeError as e:
            form.errors.setdefault(field.name, []).append(str(e))
            raise ValidationError(str(e))
        except json.JSONDecodeError as e:
            form.errors.setdefault(field.name, []).append(str(e))
            raise ValidationError(str(e))
        except jsonschema.ValidationError as e:
            if self.message:
                form.errors.setdefault(field.name, []).append(self.message)
                raise ValidationError(self.message)
            form.errors.setdefault(field.name, []).append(str(e))
            raise ValidationError(e.message)


class JsonValidator(JsonSchema):
    def __init__(self, schema, message=None):
        super(JsonValidator, self).__init__(schema, message)

    def __call__(self, data):
        try:
            data = json.loads(data)
            jsonschema.validate(data, self.schema)

            return True
        except TypeError as e:
            raise ValidationError(str(e))
        except json.JSONDecodeError as e:
            raise ValidationError(str(e))
        except jsonschema.ValidationError as e:
            if self.message:
                raise ValidationError(self.message)
            raise ValidationError(e.message)


class IsTypeValidator(object):
    """
    Validates the field to be exact type.

    bool
    int
    float
    str
    None
    etc...

    :param type:
        Variable type to check input data with it. "var_name is TYPE" will be executed
    :param message:
        Error message to raise in case of a validation error.
    """
    def __init__(self, var_type=None, message=None):
        self.message = message
        self.type = var_type

    def __call__(self, data, message=None):
        if type(data) is not self.type:
            if self.message:
                raise ValidationError(self.message)
            raise ValidationError('This field should be type of "{}"'.format(
                self.type.__name__ if hasattr(self.type, '__name__') else str(self.type)
            ))

        return True


class RegexpValidator(object):
    """
    Validates the field against a user provided regexp.

    :param regex:
        The regular expression string to use. Can also be a compiled regular
        expression pattern.
    :param flags:
        The regexp flags to use, for example re.IGNORECASE. Ignored if
        `regex` is not a string.
    :param message:
        Error message to raise in case of a validation error.
    """
    def __init__(self, regex, flags=0, message=None):
        if isinstance(regex, string_types):
            regex = re.compile(regex, flags)
        self.regex = regex
        self.message = message

    def __call__(self, data, message=None):
        match = self.regex.match(data or '')
        if not match:
            if message is None:
                if self.message is None:
                    message = 'Invalid input.'
                else:
                    message = self.message

            raise ValidationError(message)
        return match


class All(object):
    """
    Compares the incoming data to a sequence of valid inputs.

    :param validators:
        A sequence of valid inputs.
    :param message:
        Error message to raise in case of a validation error. `%(values)s`
        contains the list of values.
    :param values_formatter:
        Function used to format the list of values in the error message.
    """
    def __init__(self, validators, message=None, values_formatter=None):
        self.validators = validators
        self.message = message
        if values_formatter is None:
            values_formatter = self.default_values_formatter
        self.values_formatter = values_formatter

    def __call__(self, form, field):
        if not isinstance(field.data, list):
            raise ValidationError('Not list passed as data')

        for item in field.data:
            for validator in self.validators:
                try:
                    validator(item)
                except ValidationError as e:
                    form.errors.setdefault(field.name, []).append(str(e))
                    raise e
    @staticmethod
    def default_values_formatter(values):
        return ', '.join(text_type(x) for x in validators)
