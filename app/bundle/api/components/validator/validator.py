import collections

from future.utils import iteritems
from werkzeug.datastructures import MultiDict
from wtforms.fields import (Field)
from wtforms.form import BaseForm
from wtforms.validators import (
    DataRequired,
    NumberRange,
)


class Validator(object):
    #: flask.Request attributes available for validation
    valid_attrs = ['args', 'form', 'values', 'cookies',
                   'headers', 'json', 'rule', 'parameters']
    parameters = {}
    form = {}

    def __init__(self, request, **kwargs):
        """Base class for input validation. Subclass to add validators.
        :param request: flask.Request object to validate.
        :param object: parameters object to validate.
        """
        self.object = object()
        for key, value in kwargs.items():
            setattr(self, key, value)
        #: List of errors from all validators.
        self.errors = {}

        self._request = request
        self._forms = dict()

        for name in dir(self):
            if not name.startswith('_') and name not in ['errors', 'validate', 'valid_attrs', 'object']:
                input = getattr(self, name)
                fields = dict()
                if isinstance(input, dict):
                    for field, validators in iteritems(input):
                        fields[field] = Field(validators=validators)
                elif isinstance(input, collections.Iterable):
                    fields['_input'] = Field(validators=input)

                self._forms[name] = BaseForm(fields)

    def _get_values(self, attribute, coerse=True):
        """Compatibility function to return MultiDict objects with values from
        a flask.Request object.
        :param attribute: Request attribute to return values for.
        :param coerse: Return single input with raw data.
        :returns: werkzeug.datastructures.MultiDict
        """
        lists = {}
        if attribute in self.valid_attrs:
            if attribute == 'parameters':
                ret = {}
                obj = getattr(self, 'object')
                attrs = [attr for attr in dir(obj) if not attr.startswith('_')]
                for attr in attrs:
                    item = getattr(obj, attr)
                    if isinstance(item, (list,)):
                        lists[attr] = item
                    ret[attr] = item
            elif attribute == 'rule':
                ret = self._request.view_args
            else:
                ret = getattr(self._request, attribute)
                for key in ret.keys():
                    if key.endswith('[]'):
                        lists[key] = list(ret.getlist(key))
            if coerse:
                ret = MultiDict(ret)
                for key, value in lists.items():
                    ret[key] = value
                return ret
            else:
                return MultiDict(dict(_input=ret))

    def validate(self):
        """Validate incoming request data. Returns True if all data is valid.
        Adds each of the validator's error messages to Validator.errors if not valid.
        :returns: Boolean
        """
        for attribute, form in iteritems(self._forms):
            if '_input' in form._fields:
                form.process(self._get_values(attribute, coerse=False))
            else:
                form.process(self._get_values(attribute))
            if not form.validate():
                self.errors.update(form.errors)
                return False
        return True


class BaseActionValidator(Validator):
    def __init__(self, request, **kwargs):
        self.parameters.update({
            'action_name': [DataRequired()]
        })
        super(BaseActionValidator, self).__init__(request, **kwargs)


class PaginatedValidator(BaseActionValidator):
    parameters = {
        'page': [NumberRange(min=0)],
        'limit': [NumberRange(max=50)],
    }
