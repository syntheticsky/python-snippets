class Mapper(object):
    @staticmethod
    def map(obj: object, params: dict):
        if not isinstance(obj, object):
            raise TypeError('"obj" parameter should be a type of object but {} is given'.format(type(obj)))
        if not isinstance(params, dict):
            raise TypeError('"params" parameter should be a type of dict but {} is given'.format(type(params)))

        for name, value in params.items():
            if hasattr(obj, name) or name in vars(obj):
                setattr(obj, name, value)
