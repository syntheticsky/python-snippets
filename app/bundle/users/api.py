import datetime
import uuid

import flask
import sqlalchemy
import sqlalchemy.exc

import app
from app.bundle.api.authetication.authentication import (current_user)
from app.bundle.api.components.model_builder.model_builder import (ModelBuilder)
from app.bundle.api.exception.api_exception import (ApiException, ValidationApiException)
from app.bundle.api.exception.auth_exception import (AuthenticationError)
from app.bundle.api.server.response.response import (ApiResponse)
from app.bundle.topics.models import (Category)
from app.bundle.users.models import (User as UserEntity, UsersGroups, BlacklistToken, Group as GroupEntity)
from app.bundle.users.request.request import (AuthRequest, GroupUpdateRequest, GroupCreateRequest, LoginRequest,
                                              UserUpdateRequest, UserViewRequest, LogoutRequest, GroupInviteUserRequest)
from app.bundle.users.request.validator import (GroupCreateValidator, GroupUpdateValidator, LoginValidator,
                                                RegisterValidator, UserUpdateValidator, GroupInviteUserValidator)


@app.authRegistry.endpoint('auth.register', RegisterValidator)
def register(api_request: AuthRequest):
    """
    Action for user register
    :param api_request:
    :return:
    """
    try:
        user = UserEntity.query.filter(
            sqlalchemy.or_(UserEntity.email == api_request.email, UserEntity.username == api_request.username)
        ).one_or_none()
        if not user:
            user = UserEntity(
                username=api_request.username,
                email=api_request.email,
                password=api_request.password
            )
            # insert the user
            app.db.session.add(user)
            app.db.session.commit()

            category = Category()
            category.name = Category.UNCATEGORIZED
            category.user_id = user.id

            app.db.session.add(category)
            app.db.session.commit()

            # generate the auth token
            auth_token = user.encode_auth_token(user.id)

            return ApiResponse({'token': auth_token.decode('utf-8')}, status=201, message='Successfully registered.')
        else:
            raise ApiException(
                'User has already exists. Please Log in or try another credentials.',
                status=202
            )
    except sqlalchemy.exc.SQLAlchemyError as e:
        raise ApiException(str(e), status=400)


@app.authRegistry.endpoint('auth.login', LoginValidator)
def login(api_request: LoginRequest):
    """
    Action for user login
    :rtype: object
    :param api_request: 
    :return: 
    """
    # fetch the user data
    user = UserEntity.query.filter(
        sqlalchemy.or_(UserEntity.email == api_request.identifier, UserEntity.username == api_request.identifier)
    ).one_or_none()

    if user:
        if app.bcrypt.check_password_hash(user.password, api_request.password):
            auth_token = user.encode_auth_token(user.id)
            if auth_token:
                return ApiResponse({'token': auth_token.decode('utf-8')}, message='Successfully logged in.')
            else:
                raise ApiException('Error generating token', status=401)
        else:
            raise ValidationApiException('Wrong password.', status=401, violations={'password': ['Wrong password.']})
    else:
        raise ApiException('User not found', status=404)


@app.memberRegistry.endpoint('user.view')
def user_view(api_request: UserViewRequest):
    """
    Action for user view.
    :param api_request:
    :return:
    """
    return ApiResponse(data=ModelBuilder.build_model(current_user()))


@app.memberRegistry.endpoint('user.update', UserUpdateValidator)
def user_update(api_request: UserUpdateRequest):
    """
    Action for user update.
    :param api_request:
    :return:
    """
    try:
        user = current_user()
        if user.email != api_request.email:
            count = UserEntity.query.filter_by(email=api_request.email).count()
            if count > 0:
                raise ValidationApiException('Email already exists', status=401,
                                             violations={'email': ['Email already exists.']})
            user.email = api_request.email
        if user.username != api_request.username:
            count = UserEntity.query.filter_by(email=api_request.email).count()
            if count > 0:
                raise ValidationApiException('This username has already taken', status=401,
                                             violations={'username': ['This username has already taken']})
            user.username = api_request.username

        if api_request.password:
            user.password = UserEntity.update_password(api_request.password)

        app.db.session.commit()

        return ApiResponse(data=ModelBuilder.build_model(user))
    except AuthenticationError as e:
        app.db.session.rollback()
        raise e


@app.memberRegistry.endpoint('user.groups')
def user_groups(api_request: UserViewRequest):
    """
    Action for user groups list.
    :param api_request:
    :return:
    """
    user = current_user()
    groups = GroupEntity.query.filter(GroupEntity.user_id == user.id).all()

    return ApiResponse(data=ModelBuilder.build_models(groups))


@app.memberRegistry.endpoint('user.groups.create', GroupCreateValidator)
def user_group_create(api_request: GroupCreateRequest):
    """
    Action to create group.
    :param api_request:
    :return:
    """
    try:
        user = current_user()
        group = GroupEntity()
        group.title = api_request.title
        group.description = api_request.description
        group.updated_at = datetime.datetime.utcnow()
        group.user = user
        app.db.session.add(group)
        app.db.session.commit()

        return ApiResponse(data=ModelBuilder.build_model(group))
    except sqlalchemy.exc.SQLAlchemyError as e:
        # TODO move to validator.
        if isinstance(e, sqlalchemy.exc.IntegrityError) and -1 != str(e.orig).find(
                'duplicate key value violates unique constraint'):
            raise ValidationApiException('Title already exists', status=409,
                                         violations={'title': ['Title already exists.']})
        raise e


@app.memberRegistry.endpoint('user.groups.update', GroupUpdateValidator)
def user_group_update(api_request: GroupUpdateRequest):
    """
    Action to update group.
    :param api_request:
    :return:
    """
    try:
        user = current_user()
        group = GroupEntity.query.filter(GroupEntity.uuid == uuid.UUID(api_request.uuid)).one_or_none()

        if group is None:
            raise ApiException('Group not found with uuid "{}"'.format(api_request.uuid), status=404)
        if group.user_id != user.id:
            raise ApiException('Access denied', status=403)

        group.title = api_request.title
        group.description = api_request.description
        group.updated_at = datetime.datetime.utcnow()
        group.user = user
        app.db.session.commit()

        return ApiResponse(data=ModelBuilder.build_model(group))
    except sqlalchemy.exc.SQLAlchemyError as e:
        # TODO move to validator.
        if isinstance(e, sqlalchemy.exc.IntegrityError) and -1 != str(e.orig).find(
                'duplicate key value violates unique constraint'):
            raise ValidationApiException('Title already exists', status=409,
                                         violations={'title': ['Title already exists.']})
        raise e


@app.memberRegistry.endpoint('user.group.invite', GroupInviteUserValidator)
def user_group_invite(api_request: GroupInviteUserRequest):
    """
    Action to invite users to group
    :param api_request:
    :return:
    """
    curr_user = current_user()
    group = GroupEntity.query.filter(GroupEntity.uuid == uuid.UUID(api_request.group)).one_or_none()

    if group is None:
        raise ApiException('Group not found with uuid "{}"'.format(api_request.group), status=404)
    if group.user_id != curr_user.id:
        raise ApiException('Access denied', status=403)

    users_ids = [users_groups.user_id for users_groups in group.users.all()]

    users = UserEntity.query.join(UserEntity.user_groups).filter(sqlalchemy.and_(
        UserEntity.uuid.in_(api_request.users), UsersGroups.user_id.notin_(users_ids)
    )).group_by(UserEntity.id).all()

    if users:
        for user in users:
            user_group = UsersGroups()
            user_group.group = group
            user_group.user = user
            app.db.session.add(user_group)
        app.db.session.commit()

    return ApiResponse(data=[])


@app.memberRegistry.endpoint('user.logout')
def logout(api_request: LogoutRequest):
    """
    Action for user logout.
    :param api_request:
    :return:
    """
    # get auth token
    auth_token = flask.request.headers.get('X-Auth-Token')

    if auth_token:
        resp = UserEntity.decode_auth_token(auth_token)
        if not isinstance(resp, str):
            # mark the token as blacklisted
            blacklist_token = BlacklistToken(token=auth_token)
            try:
                # insert the token
                app.db.session.add(blacklist_token)
                app.db.session.commit()
                return ApiResponse(message='Successfully logged out.')
            except Exception as e:
                message = str(e)
                status = 500
        else:
            message = resp
            status = 401
    else:
        message = 'Provide a valid auth token.'
        status = 403
    raise ApiException(message, status=status)
