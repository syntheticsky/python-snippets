import datetime
import uuid

import jwt
from sqlalchemy.dialects import postgresql

from app import db, bcrypt, application
from app.bundle.api.components.model_builder.model_builder import (ModelBuilder)
from app.bundle.api.models import BaseModel


UserGroupsTopics = db.Table(
    'users_groups_topics',
    db.Column('group_id', db.Integer, db.ForeignKey('groups.id'), primary_key=True),
    db.Column('topic_id', db.Integer, db.ForeignKey('topics.id'), primary_key=True)
)


class UsersGroups(db.Model, BaseModel):
    __tablename__ = 'users_groups'
    __table_args__ = (
        db.Index(
            'idx_user_group',
            'user_id',
            'group_id',
            unique=True
        ),
    )

    group_id = db.Column('group_id', db.Integer, db.ForeignKey('groups.id'), primary_key=True)
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True)
    group = db.relationship('Group', back_populates='users')
    user = db.relationship('User', back_populates='user_groups')
    permission = db.Column(db.Integer, nullable=False)

    def __init__(self):
        self.permission = 0

    def get_normalized(self):
        return {
            'group_id': ['Default'],
            'user_id': ['Default'],
            # 'group': ['Default'],
            # 'user': ['Default'],
            'permission': ['Default'],
        }


class User(db.Model, BaseModel):
    """ User Model for storing user related details """
    __tablename__ = 'users'
    __table_args__ = (
        db.Index(
            'idx_user_uuid',
            'uuid',
            unique = True
        ),
        db.Index(
            'idx_user_admin',
            'admin'
        ),
    )

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    uuid = db.Column(postgresql.UUID(as_uuid=True), nullable=False)
    username = db.Column(db.String(32), unique=True, nullable=False)
    email = db.Column(db.String(64), unique=True, nullable=False)
    password = db.Column(db.String(64), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    groups = db.relationship('Group', back_populates='user', lazy='dynamic')
    user_groups = db.relationship('UsersGroups', back_populates='user', lazy='dynamic')
    topics = db.relationship('TopicsUsers', lazy='dynamic', back_populates='user')

    def __init__(self, username, email, password, admin=False):
        self.uuid = uuid.uuid1()
        self.username = username
        self.email = email
        self.password = User.update_password(password=password)
        self.created_at = datetime.datetime.utcnow()
        self.admin = admin

    @staticmethod
    def update_password(password):
        if not password:
            return

        return bcrypt.generate_password_hash(
            password, application.config.get('BCRYPT_LOG_ROUNDS', 14)
        ).decode()

    def encode_auth_token(self, user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=0),
                'iat': datetime.datetime.utcnow(),
                'iss': user_id,
                'sub': BlacklistToken.AUTH_TOKEN,
            }
            return jwt.encode(
                payload,
                application.config.get('SECRET_KEY'),
                algorithm='HS512'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, application.config.get('SECRET_KEY'))
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            elif BlacklistToken.AUTH_TOKEN != payload['sub']:
                return 'Please provide auth token. Token subject is wrong.'
            else:
                return payload['iss']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'username': ['Default'],
            'email': ['Default'],
            'admin': ['Default'],
            'created_at': ['Default'],
            'groups': ['Groups'],
        }

    def get_transformed(self, groups: list = list()):
        transformed = {}

        if self.group_intersect('uuid', groups):
            transformed['uuid'] = getattr(self.uuid, 'hex')
        if self.group_intersect('created_at', groups):
            transformed['created_at'] = str(self.created_at)
        if self.group_intersect('groups', groups):
            transformed['groups'] = list(map(lambda group: ModelBuilder.build_model(group, groups=groups), self.groups))

        return transformed


class BlacklistToken(db.Model):
    """
    Token Model for storing blacklisted JWT tokens
    """
    __tablename__ = 'tokens_blacklist'

    AUTH_TOKEN = 'AuthToken'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    blacklisted_at = db.Column(db.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_at = datetime.datetime.utcnow()

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False


class Group(db.Model, BaseModel):
    """Group model"""
    __tablename__ = 'groups'
    __table_args__ = (
        db.Index(
            'idx_user_title',
            'user_id',
            'title',
            unique=True
        ),
        db.Index(
            'idx_group_uuid',
            'uuid',
            unique=True
        )
    )

    def __init__(self):
        self.uuid = uuid.uuid1()
        self.created_at = datetime.datetime.utcnow()

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    uuid = db.Column(postgresql.UUID(as_uuid=True), nullable=False)
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)
    user = db.relationship('User', back_populates='groups', lazy='joined')
    users = db.relationship('UsersGroups', back_populates='group', lazy='dynamic')
    title = db.Column(db.String(length=255), nullable=True)
    description = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    topics = db.relationship(
        'Topic',
        lazy='dynamic',
        secondary=UserGroupsTopics,
        back_populates='groups'
    )

    def get_normalized(self):
        return {
            'id': ['Default'],
            'uuid': ['Default'],
            'user_id': ['Default'],
            'title': ['Default'],
            'description': ['Default'],
            'created_at': ['Default'],
            'updated_at': ['Default'],
        }

    def get_transformed(self, groups: list = list()):
        return {
            'uuid': getattr(self.uuid, 'hex'),
            'created_at': str(self.created_at),
            'updated_at': str(self.updated_at),
        }
