from app.bundle.api.server.request.request import BaseApiRequest


class AuthRequest(BaseApiRequest):
    def __init__(self):
        super(AuthRequest, self).__init__()
        self.__username = None
        self.__email = None
        self.__password = None

    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, value):
        self.__username = value

    @username.deleter
    def username(self):
        del self.__username

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, value):
        self.__email = value

    @email.deleter
    def email(self):
        del self.__email

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, value):
        self.__password = value

    @password.deleter
    def password(self):
        del self.__password


class LoginRequest(BaseApiRequest):
    def __init__(self):
        super(LoginRequest, self).__init__()
        self.__identifier = None
        self.__password = None

    @property
    def identifier(self):
        return self.__identifier

    @identifier.setter
    def identifier(self, value):
        self.__identifier = value

    @identifier.deleter
    def identifier(self):
        del self.__identifier

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, value):
        self.__password = value

    @password.deleter
    def password(self):
        del self.__password


class UserUpdateRequest(AuthRequest):
    pass


class UserViewRequest(BaseApiRequest):
    pass


class GroupActionUUIDRequest(BaseApiRequest):
    def __init__(self):
        super(GroupActionUUIDRequest, self).__init__()
        self.__uuid = None

    @property
    def uuid(self):
        return self.__uuid

    @uuid.setter
    def uuid(self, value):
        self.__uuid = str(value)

    @uuid.deleter
    def uuid(self):
        del self.__uuid


class GroupCreateRequest(BaseApiRequest):
    def __init__(self):
        super(GroupCreateRequest, self).__init__()
        self.__title = None
        self.__description = None

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, value):
        self.__title = value

    @title.deleter
    def title(self):
        del self.__title

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, value):
        self.__description = value

    @description.deleter
    def description(self):
        del self.__description


class GroupUpdateRequest(GroupActionUUIDRequest, GroupCreateRequest):
    def __init__(self):
        GroupActionUUIDRequest.__init__(self)
        GroupCreateRequest.__init__(self)


class LogoutRequest(BaseApiRequest):
    pass


class GroupInviteUserRequest(BaseApiRequest):
    def __init__(self):
        super(GroupInviteUserRequest, self).__init__()
        self.__users = list()
        self.__group = None

    @property
    def users(self):
        return self.__users

    @users.setter
    def users(self, value:list):
        self.__users = value

    @users.deleter
    def users(self):
        del self.__users

    @property
    def group(self):
        return self.__group

    @group.setter
    def group(self, value):
        self.__group = value

    @group.deleter
    def group(self):
        del self.__group
