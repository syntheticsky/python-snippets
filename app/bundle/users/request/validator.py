from wtforms.validators import (
    Email,
    Length,
    Optional,
    DataRequired,
    Regexp,
)

from app.bundle.api.authetication.authentication import (is_user_exists_by_email_or_username,
                                                         is_user_not_exists_by_email_or_username)
from app.bundle.api.components.validator.constraints import All, RegexpValidator
from app.bundle.api.components.validator.validator import BaseActionValidator


class LoginValidator(BaseActionValidator):
    parameters = {
        'password': [DataRequired()],
        'identifier': [DataRequired(), is_user_exists_by_email_or_username],
    }


class RegisterValidator(BaseActionValidator):
    parameters = {
        'password': [DataRequired(), Length(min=3, max=64)],
        'email': [DataRequired(), Email(), Length(max=64), is_user_not_exists_by_email_or_username],
        'username': [DataRequired(), Length(min=3, max=32), is_user_not_exists_by_email_or_username],
    }


class UserUpdateValidator(BaseActionValidator):
    parameters = {
        'password': [Optional(), Length(min=3, max=64)],
        'email': [Optional(), Email(), Length(max=64), is_user_not_exists_by_email_or_username],
        'username': [Optional(), Length(min=3, max=32), is_user_not_exists_by_email_or_username],
    }


class GroupCreateValidator(BaseActionValidator):
    parameters = {
        'title': [DataRequired()],
        'description': [Optional()],
    }


class GroupUpdateValidator(BaseActionValidator):
    parameters = {
        'uuid': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
        'title': [DataRequired()],
        'description': [Optional()],
    }


class GroupInviteUserValidator(BaseActionValidator):
    parameters = {
        'group': [DataRequired(), Regexp(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")],
        'users': [DataRequired(), All(validators=[RegexpValidator(r'^[0-9a-fA-F]{32}$', message="Invalid UUID.")])],
    }
