import os

import flask
from flask import send_from_directory, current_app


coreBlueprint = flask.Blueprint('core', __name__)

@coreBlueprint.route('/favicon.ico', methods=['GET'], strict_slashes=False)
def favicon():
    return send_from_directory(os.path.join(current_app.root_path, ".."), 'favicon.ico', mimetype='image/vnd.microsoft.icon')
