import sqlalchemy.types as types


class SimpleArray(types.TypeDecorator):
    """Simple implementation of array as string with values concatenated with comma in DB and list in code"""

    impl = types.String

    def process_bind_param(self, value, dialect):
        if not isinstance(value, list):
            raise
        return

    def process_result_value(self, value, dialect):
        return value[7:]

    def copy(self, **kw):
        return SimpleArray(self.impl.length)
