import uuid


class UniquenessKey:
    """
    Unique key mixin. Should be overridden in particular class if necessary
    """
    def get_unique_key(self):
        return uuid.uuid4().hex


class CachedKey(UniquenessKey):
    """
    Cached key mixin. Should be overridden in particular class if necessary
    """
    def get_cache_key_pattern(self):
        return self.__class__.__name__ + '/{}'

    def get_cache_key(self):
        return self.get_cache_key_pattern().format(self.get_unique_key())
