import flask_bcrypt
import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()
bcrypt = flask_bcrypt.Bcrypt()