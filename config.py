import os


class Config(object):
    # Определяет, включен ли режим отладки
    # В случае если включен, flask будет показывать
    # подробную отладочную информацию. Если выключен -
    # - 500 ошибку без какой либо дополнительной информации.
    DEBUG = False
    # Включение защиты против "Cross-site Request Forgery (CSRF)"
    CSRF_ENABLED = True
    # Случайный ключ, которые будет исползоваться для подписи
    # данных, например cookies.
    SECRET_KEY = 'h<\x8c\x84|o[\xfaId5\xbc\x1b&\xbe\xa0b^G\xc3\xd3/\x12\x07'
    # URI используемая для подключения к базе данных
#    SQLALCHEMY_DATABASE_URI = os.environ['FLASK_DATABASE_URL']
#    SQLALCHEMY_DATABASE_URI = 'postgresql://devs:Q1w2e3r4@localhost/devs'
    SQLALCHEMY_DATABASE_URI = 'postgresql://devs:Q1w2e3r4@host.docker.internal:5432/devs'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SECURITY_TOKEN_AUTHENTICATION_KEY = 'X-Auth-Token'
    SECURITY_TOKEN_AUTHENTICATION_HEADER = 'X-Auth-Token'
    SQLALCHEMY_ECHO = False

    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    UPLOAD_FOLDER = '/uploads'
    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

    # Redis config
    REDIS_URL = 'localhost'
    REDIS_PORT = 6379
    REDIS_DB = 0


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
